package com.kongzhong.finance.ghbank.gateway.controller;

import com.kongzhong.finance.ghbank.api.ICallbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zhuye on 14/02/2017.
 */
@RestController
@RequestMapping("callback")
public class CallbackController
{
    @Autowired
    private ICallbackService callbackService;

    @RequestMapping
    public String callback(@RequestBody String data)
    {
        return callbackService.callback(data);
    }
}
