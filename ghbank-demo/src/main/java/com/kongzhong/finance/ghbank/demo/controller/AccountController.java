package com.kongzhong.finance.ghbank.demo.controller;

import com.kongzhong.finance.ghbank.api.IAsyncService;
import com.kongzhong.finance.ghbank.api.params.BindCardParam;
import com.kongzhong.finance.ghbank.api.params.CreateAccountParam;
import com.kongzhong.finance.ghbank.api.params.DepositAccountParam;
import com.kongzhong.finance.ghbank.api.params.WithdrawAccountParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * Created by zhuye on 18/01/2017.
 */
@RestController
@RequestMapping("account")
public class AccountController
{
    @Autowired
    private IAsyncService asyncService;

    @RequestMapping("create")
    public String createAccount()
    {
        CreateAccountParam param = new CreateAccountParam();
        param.setAccountName("朱晔");
        param.setEmail("yzhu@live.com");
        param.setIdNo("310103198302174035");
        param.setMobile("13651657101");
        param.setReturnUrl("222");
        param.setCustomerManagerNo("");
        return asyncService.createAccount(param).getFormHtml();
    }

    @RequestMapping("bind")
    public String bindCard()
    {
        BindCardParam param = new BindCardParam();
        param.setAccountNo("111");
        param.setReturnUrl("222");
        return asyncService.bindCard(param).getFormHtml();
    }

    @RequestMapping("deposit")
    public String depositAccount()
    {
        DepositAccountParam param = new DepositAccountParam();
        param.setAccountName("葛含云");
        param.setAccountNo("6236882280000093125");
        param.setAmount(new BigDecimal("100000"));
        param.setReturnUrl("222");
        param.setRemark("setRemark");
        return asyncService.depositAccount(param).getFormHtml();
    }

    @RequestMapping("withdraw")
    public String withdrawAccount()
    {
        WithdrawAccountParam param = new WithdrawAccountParam();
        param.setAccountNo("111");
        param.setAccountName("朱晔");
        param.setReturnUrl("222");
        param.setAmount(new BigDecimal("100"));
        param.setRemark("setRemark");
        return asyncService.withdrawAccount(param).getFormHtml();
    }
}
