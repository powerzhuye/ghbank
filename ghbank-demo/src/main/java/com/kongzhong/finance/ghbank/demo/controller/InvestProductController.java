package com.kongzhong.finance.ghbank.demo.controller;

import com.kongzhong.finance.ghbank.api.IAsyncService;
import com.kongzhong.finance.ghbank.api.enums.RepayInvestProductRepayType;
import com.kongzhong.finance.ghbank.api.params.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * Created by zhuye on 18/01/2017.
 */
@RestController
@RequestMapping("product")
public class InvestProductController
{
    @Autowired
    private IAsyncService asyncService;

    @RequestMapping("purchase1")
    public String purchaseInvestProduct1()
    {
        PurchaseInvestProductParam param = new PurchaseInvestProductParam();
        param.setAccountNo("6236882280000093133");
        param.setAccountName("蒋丽");
        param.setReturnUrl("https://jr.kongzhong.com");
        param.setLoanNo("ZYTEST010");
        param.setAmount(new BigDecimal("10000"));
        param.setRemark("");
        return asyncService.purchaseInvestProduct(param).getFormHtml();
    }

    @RequestMapping("purchase2")
    public String purchaseInvestProduct2()
    {
        PurchaseInvestProductParam param = new PurchaseInvestProductParam();
        param.setAccountNo("6236882280000093109");
        param.setAccountName("苗瑞丽");
        param.setReturnUrl("https://jr.kongzhong.com");
        param.setLoanNo("ZYTEST007");
        param.setAmount(new BigDecimal("10000"));
        param.setRemark("");
        return asyncService.purchaseInvestProduct(param).getFormHtml();
    }

    @RequestMapping("transfer")
    public String transferInvestProduct()
    {
        TransferInvestProductParam param = new TransferInvestProductParam();
        param.setAccountNo("111");
        param.setAccountName("朱晔");
        param.setReturnUrl("22");
        param.setAmount(new BigDecimal("100"));
        param.setPredictIncome(new BigDecimal("5"));
        param.setOldProductId("setOldProductId");
        param.setOldProductName("setOldProductName");
        param.setOldPurchaseRequestNo("setRemark");
        param.setRemark("sss");
        return asyncService.transferInvestProduct(param).getFormHtml();
    }

    @RequestMapping("authAutoPurchase")
    public String authAutoPurchaseInvestProduct()
    {
        AuthAutoPurchaseInvestProductParam param = new AuthAutoPurchaseInvestProductParam();
        param.setAccountNo("6236882280000093109");
        param.setAccountName("苗瑞丽");
        param.setReturnUrl("");
        param.setRemark("setRemark");
        return asyncService.authAutoPurchaseInvestProduct(param).getFormHtml();
    }

    @RequestMapping("repay")
    public String repayInvestProduct()
    {
        RepayInvestProductParam param = new RepayInvestProductParam();
        param.setBwAccountName("毛启");
        param.setBwAccountNo("6236882280000093190");
        param.setAmount(new BigDecimal("11000"));
        param.setFeeAmount(new BigDecimal("1000"));
        param.setRepayType(RepayInvestProductRepayType.正常还款);
        param.setReturnUrl("http://jr.kongzhong.com");
        param.setLoanNo("ZYTEST010");
        param.setRemark("setRemark");
        return asyncService.repayInvestProduct(param).getFormHtml();
    }

    @RequestMapping("authAutoRepay")
    public String authAutoRepayInvestProduct()
    {
        AuthAutoRepayInvestProductParam param = new AuthAutoRepayInvestProductParam();
        param.setBwAccountName("朱晔");
        param.setBwAccountNo("111");
        param.setReturnUrl("222");
        param.setLoanNo("setLoanNo");
        param.setRemark("setRemark");
        return asyncService.authAutoRepayInvestProduct(param).getFormHtml();
    }
}
