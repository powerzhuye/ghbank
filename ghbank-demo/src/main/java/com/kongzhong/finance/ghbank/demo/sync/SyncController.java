package com.kongzhong.finance.ghbank.demo.sync;

import com.kongzhong.finance.ghbank.api.ISyncService;
import com.kongzhong.finance.ghbank.api.ServiceException;
import com.kongzhong.finance.ghbank.api.dtos.*;
import com.kongzhong.finance.ghbank.api.params.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zhuye on 17/03/2017.
 */
@RestController
@RequestMapping("sync")
public class SyncController
{
    @Autowired
    private ISyncService syncService;

    @ApiOperation(value = "queryAccountBalance")
    @RequestMapping(method = RequestMethod.POST, value = "queryAccountBalance")
    public QueryAccountBalanceDto queryAccountBalance(@RequestBody QueryAccountBalanceParam param) throws ServiceException
    {
        return syncService.queryAccountBalance(param);
    }

    @ApiOperation(value = "checkCreateAccountResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkCreateAccountResult")
    public CheckCreateAccountResultDto checkCreateAccountResult(@RequestBody CheckCreateAccountResultParam param) throws ServiceException
    {
        return syncService.checkCreateAccountResult(param);
    }

    @ApiOperation(value = "createInvestProduct")
    @RequestMapping(method = RequestMethod.POST, value = "createInvestProduct")
    public CreateInvestProductDto createInvestProduct(@RequestBody CreateInvestProductParam param) throws ServiceException
    {
        return syncService.createInvestProduct(param);
    }

    @ApiOperation(value = "submitRepayInvestProductIncome")
    @RequestMapping(method = RequestMethod.POST, value = "submitRepayInvestProductIncome")
    public SubmitRepayInvestProductIncomeDto submitRepayInvestProductIncome(@RequestBody SubmitRepayInvestProductIncomeParam param) throws ServiceException
    {
        return syncService.submitRepayInvestProductIncome(param);
    }

    @ApiOperation(value = "cancelPurchaseInvestProduct")
    @RequestMapping(method = RequestMethod.POST, value = "cancelPurchaseInvestProduct")
    public CancelPurchaseInvestProductDto cancelPurchaseInvestProduct(@RequestBody CancelPurchaseInvestProductParam param) throws ServiceException
    {
        return syncService.cancelPurchaseInvestProduct(param);
    }

    @ApiOperation(value = "loanInvestProduct")
    @RequestMapping(method = RequestMethod.POST, value = "loanInvestProduct")
    public LoanInvestProductDto queryAccountBalance(@RequestBody LoanInvestProductParam param) throws ServiceException
    {
        return syncService.loanInvestProduct(param);
    }

    @ApiOperation(value = "discardInvestProduct")
    @RequestMapping(method = RequestMethod.POST, value = "discardInvestProduct")
    public DiscardInvestProductDto discardInvestProduct(@RequestBody DiscardInvestProductParam param) throws ServiceException
    {
        return syncService.discardInvestProduct(param);
    }
    @ApiOperation(value = "giveBonus")
    @RequestMapping(method = RequestMethod.POST, value = "giveBonus")
    public GiveBonusDto giveBonus(@RequestBody GiveBonusParam param) throws ServiceException
    {
        return syncService.giveBonus(param);
    }

    @ApiOperation(value = "checkPurchaseInvestProductResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkPurchaseInvestProductResult")
    public CheckPurchaseInvestProductResultDto checkPurchaseInvestProductResult(@RequestBody CheckPurchaseInvestProductResultParam param) throws ServiceException
    {
        return syncService.checkPurchaseInvestProductResult(param);
    }

    @ApiOperation(value = "checkDailyTransactionResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkDailyTransactionResult")
    public CheckDailyTransactionResultDto checkDailyTransactionResult(@RequestBody CheckDailyTransactionResultParam param) throws ServiceException
    {
        return syncService.checkDailyTransactionResult(param);
    }

    @ApiOperation(value = "checkDepositAccountResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkDepositAccountResult")
    public CheckDepositAccountResultDto checkDepositAccountResult(@RequestBody CheckDepositAccountResultParam param) throws ServiceException
    {
        return syncService.checkDepositAccountResult(param);
    }

    @ApiOperation(value = "checkWithdrawAccountResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkWithdrawAccountResult")
    public CheckWithdrawAccountResultDto checkWithdrawAccountResult(@RequestBody CheckWithdrawAccountResultParam param) throws ServiceException
    {
        return syncService.checkWithdrawAccountResult(param);
    }

    @ApiOperation(value = "checkLoanInvestProductResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkLoanInvestProductResult")
    public CheckLoanInvestProductResultDto checkLoanInvestProductResult(@RequestBody CheckLoanInvestProductResultParam param) throws ServiceException
    {
        return syncService.checkLoanInvestProductResult(param);
    }

    @ApiOperation(value = "checkDiscardInvestProductResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkDiscardInvestProductResult")
    public CheckDiscardInvestProductResultDto checkDiscardInvestProductResult(@RequestBody CheckDiscardInvestProductResultParam param) throws ServiceException
    {
        return syncService.checkDiscardInvestProductResult(param);
    }

    @ApiOperation(value = "checkRepayInvestProductResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkRepayInvestProductResult")
    public CheckRepayInvestProductResultDto checkRepayInvestProductResult(@RequestBody CheckRepayInvestProductResultParam param) throws ServiceException
    {
        return syncService.checkRepayInvestProductResult(param);
    }

    @ApiOperation(value = "checkSubmitRepayInvestProductIncomeResult")
    @RequestMapping(method = RequestMethod.POST, value = "checkSubmitRepayInvestProductIncomeResult")
    public CheckSubmitRepayInvestProductIncomeResultDto checkSubmitRepayInvestProductIncomeResult(@RequestBody CheckSubmitRepayInvestProductIncomeResultParam param) throws ServiceException
    {
        return syncService.checkSubmitRepayInvestProductIncomeResult(param);
    }
}
