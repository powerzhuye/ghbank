package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncDepositAccountBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncDepositAccountMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//单笔专属账户充值(OGW00045) 异步应答
@Component
public class AsyncDepositAccountBankCallbackHandler implements ICallbackHandler<
        AsyncDepositAccountBankCallback,
        AsyncBankCallbackMessageBody<AsyncDepositAccountBankCallback>,
        AsyncDepositAccountMerchantResponse>
{


    @Override
    public AsyncDepositAccountMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncDepositAccountBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncDepositAccountMerchantResponse(context);
    }
}
