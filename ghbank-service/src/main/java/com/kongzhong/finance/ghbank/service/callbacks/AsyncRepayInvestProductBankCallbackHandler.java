package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncRepayInvestProductBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncRepayInvestProductMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//借款人单标还款 (OGW00067) 异步应答
@Component
public class AsyncRepayInvestProductBankCallbackHandler implements ICallbackHandler<
        AsyncRepayInvestProductBankCallback,
        AsyncBankCallbackMessageBody<AsyncRepayInvestProductBankCallback>,
        AsyncRepayInvestProductMerchantResponse>
{

    @Override
    public AsyncRepayInvestProductMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncRepayInvestProductBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncRepayInvestProductMerchantResponse(context);
    }
}
