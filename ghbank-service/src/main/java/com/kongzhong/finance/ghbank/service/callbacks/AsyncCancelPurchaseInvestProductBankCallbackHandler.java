package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncCancelPurchaseInvestProductBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncCancelPurchaseInvestProductMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//银行主动单笔撤标（必须）(OGW0014T)
@Component
public class AsyncCancelPurchaseInvestProductBankCallbackHandler implements ICallbackHandler<
        AsyncCancelPurchaseInvestProductBankCallback,
        AsyncBankCallbackMessageBody<AsyncCancelPurchaseInvestProductBankCallback>,
        AsyncCancelPurchaseInvestProductMerchantResponse>
{

    @Override
    public AsyncCancelPurchaseInvestProductMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncCancelPurchaseInvestProductBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncCancelPurchaseInvestProductMerchantResponse(context);
    }
}
