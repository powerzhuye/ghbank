package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncTransferInvestProductBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncTransferInvestProductMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//债券转让申请(OGW00061) 异步应答
@Component
public class AsyncTransferInvestProductBankCallbackHandler implements ICallbackHandler<
        AsyncTransferInvestProductBankCallback,
        AsyncBankCallbackMessageBody<AsyncTransferInvestProductBankCallback>,
        AsyncTransferInvestProductMerchantResponse>
{

    @Override
    public AsyncTransferInvestProductMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncTransferInvestProductBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncTransferInvestProductMerchantResponse(context);
    }
}
