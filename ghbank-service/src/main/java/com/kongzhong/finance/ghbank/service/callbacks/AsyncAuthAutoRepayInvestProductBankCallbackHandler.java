package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncAuthAutoRepayInvestProductBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncAuthAutoRepayInvestProductMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//自动还款授权 (OGW00069) 异步应答
@Component
public class AsyncAuthAutoRepayInvestProductBankCallbackHandler implements ICallbackHandler<
        AsyncAuthAutoRepayInvestProductBankCallback,
        AsyncBankCallbackMessageBody<AsyncAuthAutoRepayInvestProductBankCallback>,
        AsyncAuthAutoRepayInvestProductMerchantResponse>
{

    @Override
    public AsyncAuthAutoRepayInvestProductMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncAuthAutoRepayInvestProductBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        AsyncAuthAutoRepayInvestProductMerchantResponse response = new AsyncAuthAutoRepayInvestProductMerchantResponse(context);
        response.setRequestNo(bankResponse.getContent().getOldRequestNo());
        return response;
    }
}
