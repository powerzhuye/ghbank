package com.kongzhong.finance.ghbank.service;

import com.kongzhong.finance.ghbank.api.IAsyncService;
import com.kongzhong.finance.ghbank.api.dtos.AsyncServiceDto;
import com.kongzhong.finance.ghbank.api.params.*;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.ayncmerchantrequests.*;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantRequestMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.Message;
import lombok.extern.java.Log;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhuye on 18/01/2017.
 */
@Service
@Log
public class AsyncService implements IAsyncService
{
    @Autowired
    private MessageContextProvider messageContextProvider;

    private AsyncServiceDto getPostForm(MessageContext context, Message message)
    {
        AsyncServiceDto asyncServiceDto = new AsyncServiceDto();
        asyncServiceDto.setFormHtml(String.format("<html><body onload=\"document.ogwForm.submit()\"><form name=\"ogwForm\" method=\"post\" action=\"%s\">" +
                "<input type=\"hidden\" name=\"RequestData\" value=\"%s\"/>" +
                "<input type=\"hidden\" name=\"transCode\" value=\"%s\"/>" +
                "</form></body></html>", context.getBankUrl(), StringEscapeUtils.escapeHtml(message.toMessage()), message.getMessageBody().getHeader().getTransactionCode()));
        asyncServiceDto.setTransactionNo(message.getMessageBody().getHeader().getChannelFlow());
        log.info("异步请求>>" + asyncServiceDto);
        return asyncServiceDto;
    }

    @Override
    public AsyncServiceDto createAccount(CreateAccountParam param)
    {
        log.info("开户");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncCreateAccountMerchantRequest request = new AsyncCreateAccountMerchantRequest(context);
        request.setAccountName(param.getAccountName());
        request.setEmail(param.getEmail());
        request.setCustomerManagerNo(param.getCustomerManagerNo());
        request.setIdNo(param.getIdNo());
        request.setMobile(param.getMobile());
        request.setReturnUrl(param.getReturnUrl());
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto bindCard(BindCardParam param)
    {
        log.info("绑卡");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncBindCardMerchantRequest request = new AsyncBindCardMerchantRequest(context);
        request.setReturnUrl(param.getReturnUrl());
        request.setAccountNo(param.getAccountNo());
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto depositAccount(DepositAccountParam param)
    {
        log.info("充值");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncDepositAccountMerchantRequest request = new AsyncDepositAccountMerchantRequest(context);
        request.setAccountNo(param.getAccountNo());
        request.setAccountName(param.getAccountName());
        request.setReturnUrl(param.getReturnUrl());
        request.setRemark(param.getRemark());
        request.setAmount(String.format("%.2f", param.getAmount()));
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto withdrawAccount(WithdrawAccountParam param)
    {
        log.info("提现");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncWithdrawAccountMerchantRequest request = new AsyncWithdrawAccountMerchantRequest(context);
        request.setAccountNo(param.getAccountNo());
        request.setAccountName(param.getAccountName());
        request.setReturnUrl(param.getReturnUrl());
        request.setRemark(param.getRemark());
        request.setAmount(String.format("%.2f", param.getAmount()));
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto purchaseInvestProduct(PurchaseInvestProductParam param)
    {
        log.info("投标");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncPurchaseInvestProductMerchantRequest request = new AsyncPurchaseInvestProductMerchantRequest(context);
        request.setLoanNo(param.getLoanNo());
        request.setAccountNo(param.getAccountNo());
        request.setAccountName(param.getAccountName());
        request.setReturnUrl(param.getReturnUrl());
        request.setRemark(param.getRemark());
        request.setAmount(String.format("%.2f", param.getAmount()));
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto transferInvestProduct(TransferInvestProductParam param)
    {
        log.info("债券转让申请");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncTransferInvestProductMerchantRequest request = new AsyncTransferInvestProductMerchantRequest(context);
        request.setAccountNo(param.getAccountNo());
        request.setAccountName(param.getAccountName());
        request.setReturnUrl(param.getReturnUrl());
        request.setRemark(param.getRemark());
        request.setAmount(String.format("%.2f", param.getAmount()));
        request.setPredictIncome(String.format("%.2f", param.getPredictIncome()));
        request.setOldProductId(param.getOldProductId());
        request.setOldProductName(param.getOldProductName());
        request.setOldPurchaseRequestNo(param.getOldPurchaseRequestNo());
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto authAutoPurchaseInvestProduct(AuthAutoPurchaseInvestProductParam param)
    {
        log.info("自动投标授权");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncAuthAutoPurchaseInvestProductMerchantRequest request = new AsyncAuthAutoPurchaseInvestProductMerchantRequest(context);
        request.setAccountNo(param.getAccountNo());
        request.setAccountName(param.getAccountName());
        request.setReturnUrl(param.getReturnUrl());
        request.setRemark(param.getRemark());
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto repayInvestProduct(RepayInvestProductParam param)
    {
        log.info("还款");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncRepayInvestProductMerchantRequest request = new AsyncRepayInvestProductMerchantRequest(context);
        request.setOldRequestNo(param.getOldRequestNo());
        request.setLoanNo(param.getLoanNo());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setBwAccountName(param.getBwAccountName());
        request.setRemark(param.getRemark());
        request.setReturnUrl(param.getReturnUrl());
        request.setRepayType(String.valueOf(param.getRepayType().getCode()));
        if (param.getAmount() != null)
            request.setAmount(String.format("%.2f", param.getAmount()));
        if (param.getFeeAmount() != null)
            request.setFeeAmount(String.format("%.2f", param.getFeeAmount()));
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }

    @Override
    public AsyncServiceDto authAutoRepayInvestProduct(AuthAutoRepayInvestProductParam param)
    {
        log.info("自动还款授权");
        MessageContext context = messageContextProvider.getCurrent();
        AsyncAuthAutoRepayInvestProductMerchantRequest request = new AsyncAuthAutoRepayInvestProductMerchantRequest(context);
        request.setLoanNo(param.getLoanNo());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setBwAccountName(param.getBwAccountName());
        request.setRemark(param.getRemark());
        request.setReturnUrl(param.getReturnUrl());
        Message message = new Message(context, new AsyncMerchantRequestMessageBody(request));
        return getPostForm(context, message);
    }
}
