package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncCreateAccountBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncCreateAccountMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//账户开立(OGW00042) 异步应答
@Component
public class AsyncCreateAccountBankCallbackHandler implements ICallbackHandler<
        AsyncCreateAccountBankCallback,
        AsyncBankCallbackMessageBody<AsyncCreateAccountBankCallback>,
        AsyncCreateAccountMerchantResponse>
{

    @Override
    public AsyncCreateAccountMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncCreateAccountBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncCreateAccountMerchantResponse(context);
    }
}
