package com.kongzhong.finance.ghbank.service;

import com.kongzhong.finance.ghbank.api.ICallbackService;
import com.kongzhong.finance.ghbank.core.CallbackDispatcher;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhuye on 21/01/2017.
 */
@Service
@Log
public class CallbackService implements ICallbackService
{
    @Autowired
    private MessageContextProvider messageContextProvider;

    @Autowired
    private CallbackDispatcher callbackDispatcher;

    public String callback(String data)
    {
        log.info("收到回调数据<<" + data);
        String response = callbackDispatcher.callback(data, messageContextProvider.getCurrent());
        log.info("发出回复数据>>" + response);
        return response;
    }
}
