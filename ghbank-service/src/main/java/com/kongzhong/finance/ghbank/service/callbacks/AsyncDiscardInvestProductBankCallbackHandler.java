package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncDiscardInvestProductBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncDiscardInvestProductMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//银行主动流标（必须）(OGW0015T)
@Component
public class AsyncDiscardInvestProductBankCallbackHandler implements ICallbackHandler<
        AsyncDiscardInvestProductBankCallback,
        AsyncBankCallbackMessageBody<AsyncDiscardInvestProductBankCallback>,
        AsyncDiscardInvestProductMerchantResponse>
{

    @Override
    public AsyncDiscardInvestProductMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncDiscardInvestProductBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncDiscardInvestProductMerchantResponse(context);
    }
}
