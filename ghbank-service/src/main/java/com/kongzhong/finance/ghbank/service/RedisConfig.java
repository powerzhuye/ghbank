package com.kongzhong.finance.ghbank.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;

/**
 * Created by zhuye on 17/01/2017.
 */
@Configuration
public class RedisConfig
{
    @Value("${transactionKey}")
    private String transactionSequenceKey;

    @Bean
    public RedisAtomicLong redisTemplate(RedisConnectionFactory factory)
    {
        return new RedisAtomicLong(transactionSequenceKey, factory);
    }
}