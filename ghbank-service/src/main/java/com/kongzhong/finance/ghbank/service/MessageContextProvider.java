package com.kongzhong.finance.ghbank.service;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ApplicationType;
import com.kongzhong.finance.ghbank.core.protocol.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 18/01/2017.
 */
@Component
public class MessageContextProvider
{
    @Value("${des3Key}")
    private String des3Key;

    @Value("${merchantPrivateKey}")
    private String merchantPrivateKey;

    @Value("${bankPublicKey}")
    private String bankPublicKey;

    @Value("${channelCode}")
    private String channelCode;

    @Value("${merchantId}")
    private String merchantId;

    @Value("${merchantName}")
    private String merchantName;

    @Value("${bankUrl}")
    private String bankUrl;

    @Value("${applicationType}")
    private String applicationType;

    @Autowired
    private RedisAtomicLong redisAtomicLong;

    public MessageContext getCurrent()
    {
        return MessageContext.builder()
                .transactionId(String.valueOf(redisAtomicLong.incrementAndGet()))
                .channelCode(channelCode)
                .merchantId(merchantId)
                .merchantName(merchantName)
                .des3Key(des3Key)
                .merchantPrivateKey(merchantPrivateKey)
                .bankPublicKey(bankPublicKey)
                .bankUrl(bankUrl)
                .applicationType(ApplicationType.valueOf(applicationType).toString())
                .priority(Priority.NORMAL)
                .build();
    }
}
