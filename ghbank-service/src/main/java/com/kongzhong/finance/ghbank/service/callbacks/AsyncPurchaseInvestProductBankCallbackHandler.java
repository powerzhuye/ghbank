package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncPurchaseInvestProductBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncPurchaseInvestProductMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//单笔投标 (OGW00052)异步应答
@Component
public class AsyncPurchaseInvestProductBankCallbackHandler implements ICallbackHandler<
        AsyncPurchaseInvestProductBankCallback,
        AsyncBankCallbackMessageBody<AsyncPurchaseInvestProductBankCallback>,
        AsyncPurchaseInvestProductMerchantResponse>
{

    @Override
    public AsyncPurchaseInvestProductMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncPurchaseInvestProductBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncPurchaseInvestProductMerchantResponse(context);
    }
}
