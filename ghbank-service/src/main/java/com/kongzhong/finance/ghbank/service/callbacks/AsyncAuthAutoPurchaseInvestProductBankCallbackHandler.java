package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncAuthAutoPurchaseInvestProductBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncAuthAutoPurchaseInvestProductMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//自动投标授权(OGW00056) 异步应答
@Component
public class AsyncAuthAutoPurchaseInvestProductBankCallbackHandler implements ICallbackHandler<
        AsyncAuthAutoPurchaseInvestProductBankCallback,
        AsyncBankCallbackMessageBody<AsyncAuthAutoPurchaseInvestProductBankCallback>,
        AsyncAuthAutoPurchaseInvestProductMerchantResponse>
{

    @Override
    public AsyncAuthAutoPurchaseInvestProductMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncAuthAutoPurchaseInvestProductBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncAuthAutoPurchaseInvestProductMerchantResponse(context);
    }
}
