package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncWithdrawAccountBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncWithdrawAccountMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//单笔提现(OGW00047) 异步应答
@Component
public class AsyncWithdrawAccountBankCallbackHandler implements ICallbackHandler<
        AsyncWithdrawAccountBankCallback,
        AsyncBankCallbackMessageBody<AsyncWithdrawAccountBankCallback>,
        AsyncWithdrawAccountMerchantResponse>
{
    @Override
    public AsyncWithdrawAccountMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncWithdrawAccountBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncWithdrawAccountMerchantResponse(context);
    }
}
