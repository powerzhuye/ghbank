package com.kongzhong.finance.ghbank.service;

import com.kongzhong.finance.ghbank.api.ISyncService;
import com.kongzhong.finance.ghbank.api.ServiceException;
import com.kongzhong.finance.ghbank.api.dtos.*;
import com.kongzhong.finance.ghbank.api.enums.*;
import com.kongzhong.finance.ghbank.api.params.*;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.*;
import com.kongzhong.finance.ghbank.core.syncbankresponses.*;
import com.kongzhong.finance.ghbank.core.syncmerchantrequests.*;
import lombok.extern.java.Log;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by zhuye on 22/01/2017.
 */
@Service
@Log
public class SyncService implements ISyncService
{
    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter datetimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    @Autowired
    private MessageContextProvider messageContextProvider;

    private <T extends SyncBankResponse> ResponseHeader sendRequestGetResponse(Message message, T response) throws ServiceException
    {
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        SocketConfig socketConfig = SocketConfig.custom()
                .setTcpNoDelay(true)
                .build();
        connManager.setDefaultSocketConfig(socketConfig);
        connManager.setMaxTotal(100);
        connManager.setDefaultMaxPerRoute(100);

        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(5000)
                .setConnectTimeout(5000)
                .setConnectionRequestTimeout(5000)
                .build();

        CloseableHttpClient httpclient = HttpClients.custom()
                .setConnectionManager(connManager)
                .setDefaultRequestConfig(requestConfig)
                .build();

        String requestContent = message.toMessage();
        String responseContent = null; //"001X11          0000025629F971CD0E517BF61035609158E56ABFF59B9296F4454A6F234039A34D9C9A1BC138308B4EB095439064EC08CB51D8CD22F1C64DB19AF3FB8863D894CFFE319B4C5EEC77F8103C899EFB3C344A45BF62B64DA1E5B8C499004F4C07791D5E805910F388E528C0FC6FD046022034BE4C39F93D076AAC339A001B1B54DB3F8E08A1<?xml version=\"1.0\" encoding=\"UTF-8\"?><Document><header><channelCode>GHB</channelCode><channelFlow>OGW0120160602yL3dQM</channelFlow><channelDate>20160602</channelDate><channelTime>163827</channelTime><encryptData></encryptData><status>0</status></header><body><MERCHANTID>XJP</MERCHANTID><BANKID>GHB</BANKID><TRANSCODE>OGW00075</TRANSCODE><XMLPARA>n5/fD8rnDafoDoEeOPiwEqKYM9vNLUegdBJAtE8czfPKz2fqqFzB+31BpPVBlOHJ7dhbQVt52kRbHSjFM5nxe5q2eSqGBWja0dK+Nv78V1A0cBZMChFDA7HdmL8nNDGjCpAwU2RysBTLQaMKax8sXRBmf/WBJjdx0Yk0DdtmBy4aLcfGY4tzwl8ANIWBuT2hHRhcGr0Iiee2K37FUJw/tuAECfQV3d8CBdPjOC+yaIyK5ut1mxEBXp6ywYONuB57OgoNXOA/HZAcg7hkA2TX5q6bIpzjQSB5DLREtqIB49APRzK/n1ikDYXn6zOGe7Ocpft8lWgntE0tW0kJzVvQArdtF9YQ4jKYDvzfk0V18G8zZYV8fGju0beSpKJYPrzj++2AjmjekH6pe461fnTGBaW34JbpEflxaUVfbVuTNEEzxBEo62qSzkPRmgnfEyLdlvV09KDLeeqOESSyyvFonzncU8nL9GqP+vBu9mXvVhy95OJi4slQQVmcE4ijxOGAhrVGvyjZH9Hd2ih+0qU5ggEHfOi2eAJc7hd1Ix0/U8lOM7iXrIYHLNB1UStLLJ4CDfsWhVoFElZ/7weysLgyqsEm9NQSCQ4PdsofKLQ6GmfvlmEuGkA+3g==</XMLPARA></body></Document>";

        try
        {
            HttpPost post = new HttpPost(messageContextProvider.getCurrent().getBankUrl());
            post.setEntity(new StringEntity(requestContent));
            post.setHeader("Content-Type", "application/xml; charset=utf-8");

            log.info("发出请求到"+ messageContextProvider.getCurrent().getBankUrl()+ ">>"  + requestContent);
            try (CloseableHttpResponse httpResponse = httpclient.execute(post))
            {
                responseContent = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            }
        }
        catch (Exception ex)
        {
            throw new ServiceException("", "无法获得响应数据，原因：" + ex.getMessage());
        }
        finally
        {
            try
            {
                httpclient.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        if (StringUtils.isEmpty(responseContent))
            throw new ServiceException("", "响应数据为空");
        else
            log.info("收到响应<<" + responseContent);

        MessageContext messageContext = messageContextProvider.getCurrent();
        Message<ResponseHeader, T, SyncBankResponseMessageBody<T>> responseMessage = new Message<>(messageContext, new SyncBankResponseMessageBody<>(response))
                .fromMessage(messageContext, responseContent);
        ResponseHeader responseHeader = responseMessage.getMessageBody().getHeader();

        if (responseHeader.isOK())
        {
            if (!message.getMessageBody().getHeader().getTransactionCode().equals(responseMessage.getMessageBody().getContent().getTransactionCode()))
                log.info(String.format("响应%s的交易代码为%s和请求的交易代码%s不匹配",
                        responseMessage.getMessageBody().getContent().getClass().getName(),
                        responseMessage.getMessageBody().getContent().getTransactionCode(),
                        message.getMessageBody().getHeader().getTransactionCode()));
            return responseMessage.getMessageBody().getHeader();
        }
        else
            throw new ServiceException(responseHeader.getErrorCode(), responseHeader.getErrorMsg());
    }

    private ResultHeader getResultHeader(ResponseHeader responseHeader)
    {
        ResultHeader header = new ResultHeader();
        header.setServerFlow(responseHeader.getServerFlow());
        header.setServerTime(parseDateTime(responseHeader.getServerDate(), responseHeader.getServerTime()));
        header.setStatus(ResultStatus.parse(responseHeader.getStatus()));
        return header;
    }

    private LocalDateTime parseDateTime(String date, String time)
    {
        if (StringUtils.isNotEmpty(date) && StringUtils.isNotEmpty(time))
            return LocalDateTime.parse(date.concat(time), datetimeFormatter);
        return null;
    }

    private LocalDate parseDate(String date)
    {
        if (StringUtils.isNotEmpty(date))
            return LocalDate.parse(date, dateFormatter);
        return null;
    }

    private BigDecimal parseBigDecimal(String s)
    {
        if (StringUtils.isNotEmpty(s))
            return new BigDecimal(s);
        return null;
    }

    private Integer parseInteger(String s)
    {
        if (StringUtils.isNotEmpty(s))
            return Integer.parseInt(s);
        return null;
    }

    @Override
    public SendVerificationCodeDto sendVerificationCode(SendVerificationCodeParam param) throws ServiceException
    {
        log.info("发送验证码");
        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncSendVerificationCodeMerchantRequest request = new SyncSendVerificationCodeMerchantRequest(messageContext);
        request.setAccountNo(param.getAccountNo());
        request.setOperationType(String.valueOf(param.getOperationType().getCode()));
        request.setMobileNo(param.getMobileNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncSendVerificationCodeBankResponse response = new SyncSendVerificationCodeBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return SendVerificationCodeDto
                .builder()
                .header(getResultHeader(responseHeader))
                .messageIndex(response.getMessageIndex())
                .messageSequenceNo(response.getMessageSequenceNo())
                .build();
    }

    @Override
    public SubmitRepayInvestProductIncomeDto submitRepayInvestProductIncome(SubmitRepayInvestProductIncomeParam param) throws ServiceException
    {
        log.info("还款收益明细提交");
        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncSubmitRepayInvestProductIncomeMerchantRequest request = new SyncSubmitRepayInvestProductIncomeMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());
        request.setBwAccountName(param.getBwAccountName());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setLoanNo(param.getLoanNo());
        request.setTotalCount(String.valueOf(param.getTotalCount()));
        request.setRepayType(String.valueOf(param.getRepayType().getCode()));

        request.setRepayItemList(param.getItems().stream().map(item ->
        {
            SyncSubmitRepayInvestProductIncomeMerchantRequestRepayItem repayItem = new SyncSubmitRepayInvestProductIncomeMerchantRequestRepayItem();
            repayItem.setAccountName(item.getAccountName());
            repayItem.setAccountNo(item.getAccountNo());
            repayItem.setAmount(String.format("%.2f", item.getAmount()));
            repayItem.setFeeAmount(String.format("%.2f", item.getFeeAmount()));
            repayItem.setIncomeAmount(String.format("%.2f", item.getIncomeAmount()));
            repayItem.setPrincipalAmount(String.format("%.2f", item.getPrincipalAmount()));
            repayItem.setIncomeDate(dateFormatter.format(item.getIncomeDate()));
            repayItem.setSubSequenceNo(item.getSubSequenceNo());
            return repayItem;
        }).collect(Collectors.toList()));

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncSubmitRepayInvestProductIncomeBankResponse response = new SyncSubmitRepayInvestProductIncomeBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return SubmitRepayInvestProductIncomeDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .build();
    }

    @Override
    public SubmitInvestProductDiscountDto submitInvestProductDiscount(SubmitInvestProductDiscountParam param) throws ServiceException
    {
        log.info("投标优惠返回");
        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncSubmitInvestProductDiscountMerchantRequest request = new SyncSubmitInvestProductDiscountMerchantRequest(messageContext);
        request.setBwAccountName(param.getBwAccountName());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setLoanNo(param.getLoanNo());
        request.setTotalCount(String.valueOf(param.getTotalCount()));
        request.setAmount(String.format("%.2f", param.getAmount()));

        request.setItems(param.getItems().stream().map(item ->
        {
            SyncSubmitInvestProductDiscountMerchantRequestItem discountMerchantRequestItem = new SyncSubmitInvestProductDiscountMerchantRequestItem();
            discountMerchantRequestItem.setAccountName(item.getAccountName());
            discountMerchantRequestItem.setAccountNo(item.getAccountNo());
            discountMerchantRequestItem.setAmount(String.format("%.2f", item.getAmount()));
            discountMerchantRequestItem.setOldRequestNo(item.getOldRequestNo());
            discountMerchantRequestItem.setSubSequenceNo(item.getSubSequenceNo());
            discountMerchantRequestItem.setRemark(item.getRemark());
            return discountMerchantRequestItem;
        }).collect(Collectors.toList()));

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncSubmitInvestProductDiscountBankResponse response = new SyncSubmitInvestProductDiscountBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return SubmitInvestProductDiscountDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .build();
    }

    @Override
    public CheckSubmitRepayInvestProductIncomeResultDto checkSubmitRepayInvestProductIncomeResult(CheckSubmitRepayInvestProductIncomeResultParam param) throws ServiceException
    {
        log.info("还款收益结果查询");
        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckSubmitRepayInvestProductIncomeResultMerchantRequest request = new SyncCheckSubmitRepayInvestProductIncomeResultMerchantRequest(messageContext);
        request.setLoanNo(param.getLoanNo());
        request.setSubSequenceNo(param.getSubSequenceNo());
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckSubmitRepayInvestProductIncomeResultBankResponse response = new SyncCheckSubmitRepayInvestProductIncomeResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        CheckSubmitRepayInvestProductIncomeResultDto dto = new CheckSubmitRepayInvestProductIncomeResultDto();
        dto.setHeader(getResultHeader(responseHeader));
        dto.setBwAccountName(response.getBwAccountName());
        dto.setLoanNo(response.getLoanNo());
        dto.setBwAccountNo(response.getBwAccountNo());
        dto.setErrorMessage(response.getErrorMessage());
        dto.setOldRequestNo(response.getOldRequestNo());
        dto.setTotalCount(parseInteger(response.getTotalCount()));
        dto.setRepayType(SubmitRepayInvestProductIncomeRepayType.parse(parseInteger(response.getRepayType())));
        dto.setStatus(CheckSubmitRepayInvestProductIncomeResultStatus.parse(response.getReturnStatus()));
        if (!CollectionUtils.isEmpty(response.getRepayItemList()))
        {
            dto.setItems(response.getRepayItemList().stream().map(item ->
            {
                CheckSubmitRepayInvestProductIncomeResultItemDto itemDto = new CheckSubmitRepayInvestProductIncomeResultItemDto();
                itemDto.setAccountName(item.getAccountName());
                itemDto.setAccountNo(item.getAccountNo());
                itemDto.setAmount(parseBigDecimal(item.getAmount()));
                itemDto.setIncomeAmount(parseBigDecimal(item.getIncomeAmount()));
                itemDto.setFeeAmount(parseBigDecimal(item.getFeeAmount()));
                itemDto.setPrincipalAmount(parseBigDecimal(item.getPrincipalAmount()));
                itemDto.setErrorMessage(item.getErrorMessage());
                itemDto.setSubSequenceNo(item.getSubSequenceNo());
                itemDto.setBankTransactionNo(item.getBankTransactionNo());
                itemDto.setTransactionDate(parseDate(item.getTransactionDate()));
                itemDto.setIncomeDate(parseDate(item.getIncomeDate()));
                itemDto.setStatus(CheckSubmitRepayInvestProductIncomeResultItemStatus.parse(item.getStatus()));
                return itemDto;
            }).collect(Collectors.toList()));
        }
        return dto;
    }

    @Override
    public CheckSubmitInvestProductDiscountResultDto checkSubmitInvestProductDiscountResult(CheckSubmitInvestProductDiscountResultParam param) throws ServiceException
    {
        log.info("投标优惠返回结果查询");
        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckSubmitInvestProductDiscountResultMerchantRequest request = new SyncCheckSubmitInvestProductDiscountResultMerchantRequest(messageContext);
        request.setSubSequenceNo(param.getSubSequenceNo());
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckSubmitInvestProductDiscountResultBankResponse response = new SyncCheckSubmitInvestProductDiscountResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        CheckSubmitInvestProductDiscountResultDto dto = new CheckSubmitInvestProductDiscountResultDto();
        dto.setHeader(getResultHeader(responseHeader));
        dto.setBwAccountName(response.getBwAccountName());
        dto.setBwAccountNo(response.getBwAccountNo());
        dto.setErrorMessage(response.getErrorMessage());
        dto.setTotalCount(parseInteger(response.getTotalCount()));
        dto.setLoanNo(response.getLoanNo());
        dto.setTransactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()));
        dto.setTransactionSequenceNo(response.getBankTransactionNo());
        dto.setStatus(CheckSubmitInvestProductDiscountResultStatus.parse(response.getReturnStatus()));

        if (!CollectionUtils.isEmpty(response.getItems()))
        {
            dto.setItems(response.getItems().stream().map(item ->
            {
                CheckSubmitInvestProductDiscountResultItemDto itemDto = new CheckSubmitInvestProductDiscountResultItemDto();
                itemDto.setAccountName(item.getAccountName());
                itemDto.setAccountNo(item.getAccountNo());
                itemDto.setAmount(parseBigDecimal(item.getAmount()));
                itemDto.setErrorMessage(item.getErrorMessage());
                itemDto.setSubSequenceNo(item.getSubSequenceNo());
                itemDto.setRemark(item.getRemark());
                itemDto.setOldRequestNo(item.getOldRequestNo());
                itemDto.setStatus(CheckSubmitInvestProductDiscountResultItemStatus.parse(item.getStatus()));
                return itemDto;
            }).collect(Collectors.toList()));
        }
        return dto;
    }

    @Override
    public CreateInvestProductDto createInvestProduct(CreateInvestProductParam param) throws ServiceException
    {
        log.info("单笔发标信息通知");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCreateInvestProductMerchantRequest request = new SyncCreateInvestProductMerchantRequest(messageContext);
        request.setBorrowerItemList(new ArrayList<>());
        request.setBorrowerTotalCount("1");
        request.setInvestBeginDate(dateFormatter.format(param.getInvestBeginDate()));
        request.setInvestEndDate(dateFormatter.format(param.getInvestEndDate()));
        request.setRepayDate(dateFormatter.format(param.getRepayDate()));
        request.setInvestId(param.getLoanNo());
        request.setLoanNo(param.getLoanNo());
        request.setIsTransfer(param.isTransfer() ? "1" : "0");
        request.setInvestRange(String.valueOf(param.getInvestRangeDays()));
        request.setMaxInvestAmount(String.format("%.2f", param.getMaxInvestAmount()));
        request.setMinInvestAmount(String.format("%.2f", param.getMinInvestAmount()));
        request.setInvestTotalAmount(String.format("%.2f", param.getInvestTotalAmount()));
        request.setYearRate(param.getYearRate().toString());
        request.setProductName(param.getProductName());
        request.setProductDesc(param.getProductDesc());
        request.setProductState("0");//正常
        request.setRemark(param.getRemark());
        request.setRateType(param.getRateType());
        request.setRepayType(param.getRepayType());
        request.setRefLoanNo(param.getRefLoanNo());
        request.setOldRequestSequence(param.getOldRequestSequence());

        CreateInvestProductBorrowerParam borrower = param.getBorrower();
        SyncCreateInvestProductMerchantRequestBorrowerItem borrowerItem = new SyncCreateInvestProductMerchantRequestBorrowerItem();
        borrowerItem.setAccountName(borrower.getAccountName());
        borrowerItem.setAccountNo(borrower.getAccountNo());
        borrowerItem.setRemark(borrower.getRemark());
        borrowerItem.setAmount(String.format("%.2f", borrower.getAmount()));
        borrowerItem.setBankId(borrower.getBankId());
        borrowerItem.setBankName(borrower.getBankName());
        borrowerItem.setCheckDate(dateFormatter.format(borrower.getCheckDate()));
        borrowerItem.setIdNo(borrower.getIdNo());
        borrowerItem.setMortgageId(borrower.getMortgageId());
        borrowerItem.setMortgageDesc(borrower.getMortgageDesc());
        request.getBorrowerItemList().add(borrowerItem);

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCreateInvestProductBankResponse response = new SyncCreateInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CreateInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).transactionSequenceNo(response.getBankTransactionNo())
                .build();
    }

    @Override
    public CancelPurchaseInvestProductDto cancelPurchaseInvestProduct(CancelPurchaseInvestProductParam param) throws ServiceException
    {
        log.info("单笔撤标");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCancelPurchaseInvestProductMerchantRequest request = new SyncCancelPurchaseInvestProductMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());
        request.setAccountName(param.getAccountName());
        request.setAccountNo(param.getAccountNo());
        request.setCancelReason(param.getCancelReason());
        request.setLoanNo(param.getLoanNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCancelPurchaseInvestProductBankResponse response = new SyncCancelPurchaseInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CancelPurchaseInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).build();
    }

    @Override
    public LoanInvestProductDto loanInvestProduct(LoanInvestProductParam param) throws ServiceException
    {
        log.info("放款");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncLoanInvestProductMerchantRequest request = new SyncLoanInvestProductMerchantRequest(messageContext);
        request.setBwAccountName(param.getBwAccountName());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setLoanNo(param.getLoanNo());
        request.setRemark(param.getRemark());
        request.setAccountManageAmount(param.getAccountManageAmount().toString());
        request.setGuaranteeAmount(param.getGuaranteeAmount().toString());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncLoanInvestProductBankResponse response = new SyncLoanInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return LoanInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .build();
    }

    @Override
    public DiscardInvestProductDto discardInvestProduct(DiscardInvestProductParam param) throws ServiceException
    {
        log.info("流标");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncDiscardInvestProductMerchantRequest request = new SyncDiscardInvestProductMerchantRequest(messageContext);
        request.setLoanNo(param.getLoanNo());
        request.setCancelReason(param.getCancelReason());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncDiscardInvestProductBankResponse response = new SyncDiscardInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return DiscardInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).build();
    }

    @Override
    public GiveBonusDto giveBonus(GiveBonusParam param) throws ServiceException
    {
        log.info("单笔奖励或分红");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncGiveBonusMerchantRequest request = new SyncGiveBonusMerchantRequest(messageContext);
        request.setAccountName(param.getAccountName());
        request.setAccountNo(param.getAccountNo());
        request.setAmount(String.format("%.2f", param.getAmount()));
        request.setRemark(param.getRemark());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncGiveBonusBankResponse response = new SyncGiveBonusBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return GiveBonusDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).build();
    }

    @Override
    public QueryAccountBalanceDto queryAccountBalance(QueryAccountBalanceParam param) throws ServiceException
    {
        log.info("账户余额查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncQueryAccountBalanceMerchantRequest request = new SyncQueryAccountBalanceMerchantRequest(messageContext);
        request.setAccountName(param.getAccountName());
        request.setAccountNo(param.getAccountNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncQueryAccountBalanceBankResponse response = new SyncQueryAccountBalanceBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return QueryAccountBalanceDto
                .builder()
                .header(getResultHeader(responseHeader))
                .accountBalance(parseBigDecimal(response.getAccountBalance()))
                .availableBalance(parseBigDecimal(response.getAvailableBalance()))
                .frozenBalance(parseBigDecimal(response.getFrozenBalance()))
                .accountName(response.getAccountName())
                .accountNo(response.getAccountNo())
                .accountStatus(QueryAccountBalanceAccountStatus.parse(response.getAccountStatus()))
                .build();
    }

    @Override
    public CheckDepositAccountResultDto checkDepositAccountResult(CheckDepositAccountResultParam param) throws ServiceException
    {
        log.info("单笔充值结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckDepositAccountResultMerchantRequest request = new SyncCheckDepositAccountResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckDepositAccountResultBankResponse response = new SyncCheckDepositAccountResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckDepositAccountResultDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckDepositAccountResultStatus.parse(response.getReturnStatus()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .build();
    }

    @Override
    public CheckWithdrawAccountResultDto checkWithdrawAccountResult(CheckWithdrawAccountResultParam param) throws ServiceException
    {
        log.info("单笔提现结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckWithdrawAccountResultMerchantRequest request = new SyncCheckWithdrawAccountResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());
        request.setTransactionDate(dateFormatter.format(param.getTransactionDate()));

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckWithdrawAccountResultBankResponse response = new SyncCheckWithdrawAccountResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckWithdrawAccountResultDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckWithDrawAccountResultStatus.parse(response.getReturnStatus()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .build();
    }

    @Override
    public CheckLoanInvestProductResultDto checkLoanInvestProductResult(CheckLoanInvestProductResultParam param) throws ServiceException
    {
        log.info("放款结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckLoanInvestProductResultMerchantRequest request = new SyncCheckLoanInvestProductResultMerchantRequest(messageContext);
        request.setLoanNo(param.getLoanNo());
        request.setOldPurchaseProductNo(param.getOldPurchaseProductNo());
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckLoanInvestProductResultBankResponse response = new SyncCheckLoanInvestProductResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        CheckLoanInvestProductResultDto dto = new CheckLoanInvestProductResultDto();
        dto.setHeader(getResultHeader(responseHeader));
        dto.setAccountManageAmount(parseBigDecimal(response.getAccountManageAmount()));
        dto.setGuaranteeAmount(parseBigDecimal(response.getGuaranteeAmount()));
        dto.setBwAccountName(response.getBwAccountName());
        dto.setBwAccountNo(response.getBwAccountNo());
        dto.setErrorMessage(response.getErrorMessage());
        dto.setLoanNo(response.getLoanNo());
        dto.setOldRequestNo(response.getOldRequestNo());
        dto.setStatus(CheckLoanInvestProductResultStatus.parse(response.getReturnStatus()));
        if (!CollectionUtils.isEmpty(response.getRsItems()))
        {
            dto.setItems(response.getRsItems().stream().map(item ->
            {
                CheckLoanInvestProductResultItemDto investProductResultDtoItem = new CheckLoanInvestProductResultItemDto();
                investProductResultDtoItem.setAccountName(item.getAccountName());
                investProductResultDtoItem.setAccountNo(item.getAccountNo());
                investProductResultDtoItem.setAmount(parseBigDecimal(item.getAmount()));
                investProductResultDtoItem.setLoanNo(item.getLoanNo());
                investProductResultDtoItem.setErrorMessage(item.getErrorMessage());
                investProductResultDtoItem.setRequestNo(item.getRequestNo());
                investProductResultDtoItem.setStatus(CheckLoanInvestProductResultRsItemStatus.parse(item.getStatus()));
                return investProductResultDtoItem;
            }).collect(Collectors.toList()));
        }
        return dto;
    }

    @Override
    public CheckDiscardInvestProductResultDto checkDiscardInvestProductResult(CheckDiscardInvestProductResultParam param) throws ServiceException
    {
        log.info("流标结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckDiscardInvestProductResultMerchantRequest request = new SyncCheckDiscardInvestProductResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());
        request.setOldPurchaseProductNo(param.getOldPurchaseProductNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckDiscardInvestProductResultBankResponse response = new SyncCheckDiscardInvestProductResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        CheckDiscardInvestProductResultDto dto = new CheckDiscardInvestProductResultDto();
        dto.setHeader(getResultHeader(responseHeader));
        dto.setOldRequestNo(response.getOldRequestNo());
        dto.setStatus(CheckDiscardInvestProductResultStatus.parse(response.getReturnStatus()));
        if (!CollectionUtils.isEmpty(response.getRsItems()))
        {
            dto.setItems(response.getRsItems().stream().map(item ->
            {
                CheckDiscardInvestProductResultItemDto discardInvestProductResultItemDto = new CheckDiscardInvestProductResultItemDto();
                discardInvestProductResultItemDto.setAccountName(item.getAccountName());
                discardInvestProductResultItemDto.setAccountNo(item.getAccountNo());
                discardInvestProductResultItemDto.setBankTransactionNo(item.getBankTransactionNo());
                discardInvestProductResultItemDto.setAmount(parseBigDecimal(item.getAmount()));
                discardInvestProductResultItemDto.setErrorMessage(item.getErrorMessage());
                discardInvestProductResultItemDto.setLoanNo(item.getLoanNo());
                discardInvestProductResultItemDto.setRequestNo(item.getRequestNo());
                discardInvestProductResultItemDto.setTransactionDate(parseDate(item.getTransactionDate()));
                discardInvestProductResultItemDto.setStatus(CheckDiscardInvestProductResultItemStatus.parse(item.getStatus()));
                return discardInvestProductResultItemDto;
            }).collect(Collectors.toList()));
        }
        return dto;
    }

    @Override
    public CheckRepayInvestProductResultDto checkRepayInvestProductResult(CheckRepayInvestProductResultParam param) throws ServiceException
    {
        log.info("借款人单笔还款结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckRepayInvestProductResultMerchantRequest request = new SyncCheckRepayInvestProductResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckRepayInvestProductResultBankResponse response = new SyncCheckRepayInvestProductResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckRepayInvestProductResultDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckRepayInvestProductResultStatus.parse(response.getReturnStatus()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .bwAccountName(response.getBwAccountName())
                .bwAccountNo(response.getBwAccountNo())
                .amount(parseBigDecimal(response.getAmount()))
                .build();
    }

    @Override
    public CheckAuthAutoRepayInvestProductResultDto checkAuthAutoRepayInvestProductResult(CheckAuthAutoRepayInvestProductResultParam param) throws ServiceException
    {
        log.info("自动还款授权结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckAuthAutoRepayInvestProductResultMerchantRequest request = new SyncCheckAuthAutoRepayInvestProductResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckRepayInvestProductResultBankResponse response = new SyncCheckRepayInvestProductResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckAuthAutoRepayInvestProductResultDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckAuthAutoRepayInvestProductResultStatus.parse(response.getReturnStatus()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .build();
    }

    @Override
    public CheckAuthAutoPurchaseInvestProductDto checkAuthAutoPurchaseInvestProductResult(CheckAuthAutoPurchaseInvestProductResultParam param) throws ServiceException
    {
        log.info("自动投标授权结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckAuthAutoPurchaseInvestProductResultMerchantRequest request = new SyncCheckAuthAutoPurchaseInvestProductResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckPurchaseInvestProductResultBankResponse response = new SyncCheckPurchaseInvestProductResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckAuthAutoPurchaseInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckAuthAutoPurchaseInvestProductResultStatus.parse(response.getReturnStatus()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .build();
    }

    @Override
    public CancelAuthAutoPurchaseInvestProductDto cancelAuthAutoPurchaseInvestProduct(CancelAuthAutoPurchaseInvestProductParam param) throws ServiceException
    {
        log.info("自动投标授权撤销");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCancelAuthAutoPurchaseInvestProductMerchantRequest request = new SyncCancelAuthAutoPurchaseInvestProductMerchantRequest(messageContext);
        request.setAccountName(param.getAccountName());
        request.setAccountNo(param.getAccountNo());
        request.setMessageNo(param.getMessageNo());
        request.setMessageSequenceNo(param.getMessageSequenceNo());
        request.setRemark(param.getRemark());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCancelAuthAutoPurchaseInvestProductBankResponse response = new SyncCancelAuthAutoPurchaseInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CancelAuthAutoPurchaseInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .build();
    }

    @Override
    public CancelAuthAutoRepayInvestProductDto cancelAuthAutoRepayInvestProduct(CancelAuthAutoRepayInvestProductParam param) throws ServiceException
    {
        log.info("自动还款授权撤销");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCancelAuthAutoRepayInvestProductMerchantRequest request = new SyncCancelAuthAutoRepayInvestProductMerchantRequest(messageContext);
        request.setBwAccountName(param.getBwAccountName());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setLoanNo(param.getLoanNo());
        request.setMessageNo(param.getMessageNo());
        request.setMessageSequenceNo(param.getMessageSequenceNo());
        request.setRemark(param.getRemark());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCancelAuthAutoRepayInvestProductBankResponse response = new SyncCancelAuthAutoRepayInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CancelAuthAutoRepayInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .build();
    }

    @Override
    public CheckCreateAccountResultDto checkCreateAccountResult(CheckCreateAccountResultParam param) throws ServiceException
    {
        log.info("账户开立结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckCreateAccountResultMerchantRequest request = new SyncCheckCreateAccountResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckCreateAccountResultBankResponse response = new SyncCheckCreateAccountResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckCreateAccountResultDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckCreateAccountResultStatus.parse(response.getReturnStatus()))
                .accountName(response.getAccountName())
                .accountNo(response.getAccountNo())
                .idNo(response.getIdNo())
                .idType(response.getIdType())
                .mobile(response.getMobile())
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .build();
    }

    @Override
    public CheckPurchaseInvestProductResultDto checkPurchaseInvestProductResult(CheckPurchaseInvestProductResultParam param) throws ServiceException
    {
        log.info("单笔投标结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckPurchaseInvestProductResultMerchantRequest request = new SyncCheckPurchaseInvestProductResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckPurchaseInvestProductResultBankResponse response = new SyncCheckPurchaseInvestProductResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckPurchaseInvestProductResultDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckPurchaseInvestProductResultStatus.parse(response.getReturnStatus()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .build();
    }

    @Override
    public CheckTransferInvestProductResultDto checkTransferInvestProductResult(CheckTransferInvestProductResultParam param) throws ServiceException
    {
        log.info("债权转让结果查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckTransferInvestProductResultMerchantRequest request = new SyncCheckTransferInvestProductResultMerchantRequest(messageContext);
        request.setOldRequestNo(param.getOldRequestNo());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckTransferInvestProductResultBankResponse response = new SyncCheckTransferInvestProductResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CheckTransferInvestProductResultDto
                .builder()
                .header(getResultHeader(responseHeader))
                .status(CheckTransferInvestProductResultStatus.parse(response.getReturnStatus()))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime())).errorMessage(response.getErrorMessage())
                .oldRequestNo(response.getOldRequestNo())
                .build();
    }

    @Override
    public AutoRepayInvestProductDto autoRepayInvestProduct(AutoRepayInvestProductParam param) throws ServiceException
    {
        log.info("自动单笔还款");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncAutoRepayInvestProductMerchantRequest request = new SyncAutoRepayInvestProductMerchantRequest(messageContext);
        request.setAmount(String.format("%.2f", param.getAmount()));
        request.setFeeAmount(String.format("%.2f", param.getFeeAmount()));
        request.setBwAccountName(param.getBwAccountName());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setLoanNo(param.getLoanNo());
        request.setRemark(param.getRemark());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncAutoRepayInvestProductBankResponse response = new SyncAutoRepayInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return AutoRepayInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .build();
    }

    @Override
    public AutoPurchaseInvestProductDto autoPurchaseInvestProduct(AutoPurchaseInvestProductParam param) throws ServiceException
    {
        log.info("自动单笔投标");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncAutoPurchaseInvestProductMerchantRequest request = new SyncAutoPurchaseInvestProductMerchantRequest(messageContext);
        request.setAmount(String.format("%.2f", param.getAmount()));
        request.setAccountName(param.getAccountName());
        request.setAccountNo(param.getAccountNo());
        request.setLoanNo(param.getLoanNo());
        request.setRemark(param.getRemark());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncAutoPurchaseInvestProductBankResponse response = new SyncAutoPurchaseInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return AutoPurchaseInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .build();
    }

    @Override
    public CompanyRepayInvestProductDto companyRepayInvestProduct(CompanyRepayInvestProductParam param) throws ServiceException
    {
        log.info("单标公司垫付还款");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCompanyRepayInvestProductMerchantRequest request = new SyncCompanyRepayInvestProductMerchantRequest(messageContext);
        request.setAmount(String.format("%.2f", param.getAmount()));
        request.setFeeAmount(String.format("%.2f", param.getFeeAmount()));
        request.setLoanNo(param.getLoanNo());
        request.setBwAccountName(param.getBwAccountName());
        request.setBwAccountNo(param.getBwAccountNo());
        request.setRemark(param.getRemark());

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCompanyRepayInvestProductBankResponse response = new SyncCompanyRepayInvestProductBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        return CompanyRepayInvestProductDto
                .builder()
                .header(getResultHeader(responseHeader))
                .transactionSequenceNo(response.getBankTransactionNo())
                .transactionDatetime(parseDateTime(response.getTransactionDate(), response.getTransactionTime()))
                .build();
    }

    @Override
    public CheckDailyTransactionResultDto checkDailyTransactionResult(CheckDailyTransactionResultParam param) throws ServiceException
    {
        log.info("日终对账请求查询");

        MessageContext messageContext = messageContextProvider.getCurrent();
        SyncCheckDailyTransactionResultMerchantRequest request = new SyncCheckDailyTransactionResultMerchantRequest(messageContext);
        request.setCheckDate(dateFormatter.format(param.getCheckDate()));

        Message message = new Message(messageContext, new SyncMerchantRequestMessageBody(request));
        SyncCheckDailyTransactionResultBankResponse response = new SyncCheckDailyTransactionResultBankResponse();
        ResponseHeader responseHeader = sendRequestGetResponse(message, response);

        CheckDailyTransactionResultDto dto = new CheckDailyTransactionResultDto();
        dto.setHeader(getResultHeader(responseHeader));
        dto.setStatus(CheckDailyTransactionResultStatus.parse(response.getReturnStatus()));
        dto.setFileName(response.getFileName());
        dto.setErrorMessage(response.getErrorMessage());
        dto.setOldRequestNo(response.getOldRequestNo());
        dto.setItems(new ArrayList<>());
        if (StringUtils.isNotEmpty(response.getFileContent()) && dto.getStatus() == CheckDailyTransactionResultStatus.可对账)
        {
            String s = response.getFileContent()
                    .replace("\n", "").replace(" ", "");
            byte[] zipData = Base64.decodeBase64(s);
            String content = "";
            String directoryName = "data";
            try
            {
                FileOutputStream fos = new FileOutputStream(dto.getFileName());
                fos.write(zipData);
                fos.close();
                net.lingala.zip4j.core.ZipFile zipFile = new net.lingala.zip4j.core.ZipFile(dto.getFileName());
                zipFile.extractAll(directoryName);
                FileUtils.forceDelete(new File(dto.getFileName()));
                content = FileUtils.readFileToString(new File(directoryName + "/" + dto.getFileName().replace(".zip", ".txt")), "GBK");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


            //String content = Utils.decompress(zipData, "GBK");
            if (StringUtils.isNotEmpty(content))
            {
                String[] lines = content.split(System.getProperty("line.separator"));
                if (lines.length > 0)
                {
                    String summary = lines[0];
                    if (StringUtils.isNotEmpty(summary))
                    {
                        String[] columns = summary.split("\\|");
                        if (columns.length == 3)
                        {
                            dto.setMerchantNo(columns[0]);
                            dto.setCheckDate(parseDate(columns[1]));
                            dto.setTransactionCount(parseInteger(columns[2]));
                        }
                    }
                    for (int i = 1; i < lines.length; i++)
                    {
                        CheckDailyTransactionResultItemDto item = new CheckDailyTransactionResultItemDto();
                        String line = lines[i];
                        String[] columns = line.split("\\|");
                        if (columns.length == 18)
                        {
                            try
                            {
                                item.setType(CheckDailyTransactionResultDtoItemType.parse(parseInteger(columns[0])));
                                item.setParentTransactionNo(columns[1]);
                                item.setChildTransactionNo(columns[2]);
                                item.setLoanNo(columns[3]);
                                item.setPayAccountNo(columns[4]);
                                item.setPayAccountName(columns[5]);
                                item.setPayAccountBankId(columns[6]);
                                item.setPayAccountBankName(columns[7]);
                                item.setReceiveAccountNo(columns[8]);
                                item.setReceiveAccountName(columns[9]);
                                item.setReceiveAccountBankId(columns[10]);
                                item.setReceiveAccountBankName(columns[11]);
                                item.setAmount(parseBigDecimal(columns[12]));
                                item.setFeeAmount(parseBigDecimal(columns[13]));
                                item.setManagementAmount(parseBigDecimal(columns[14]));
                                item.setGuaranteeAmount(parseBigDecimal(columns[15]));
                                item.setBankSettleDate(parseDate(columns[16]));
                                item.setBankSettleTransactionNo(columns[17]);
                                dto.getItems().add(item);
                            }
                            catch (Exception ex)
                            {
                                throw new ServiceException("", String.format("第%d行对账信息格式不对:%s,错误信息:%s", i, line, ex.getMessage()));
                            }
                        }
                        else
                        {
                            throw new ServiceException("", String.format("第%d行对账信息格式不对:%s", i, line));
                        }
                    }
                }
            }
        }
        return dto;
    }
}
