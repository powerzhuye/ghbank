package com.kongzhong.finance.ghbank.service.callbacks;

import com.kongzhong.finance.ghbank.core.ICallbackHandler;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.AsyncBindCardBankCallback;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.AsyncBindCardMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;
import org.springframework.stereotype.Component;

/**
 * Created by zhuye on 22/01/2017.
 */
//绑卡(OGW00044)异步应答
@Component
public class AsyncBindCardBankCallbackHandler implements ICallbackHandler<
        AsyncBindCardBankCallback,
        AsyncBankCallbackMessageBody<AsyncBindCardBankCallback>,
        AsyncBindCardMerchantResponse>
{

    @Override
    public AsyncBindCardMerchantResponse handle(AsyncBankCallbackMessageBody<AsyncBindCardBankCallback> bankResponse, MessageContext context) throws ResponseException
    {
        return new AsyncBindCardMerchantResponse(context);
    }
}
