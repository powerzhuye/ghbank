package com.kongzhong.finance.ghbank.service;

import com.kongzhong.finance.ghbank.core.Utils;
import lombok.extern.java.Log;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import sun.misc.BASE64Encoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import static org.joox.JOOX.$;

/**
 * Created by zhuye on 17/01/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@Log
public class ProtocolTest
{
    static String data = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Document><header><encryptData></encryptData><channelCode>GHB </channelCode><transCode>OGW00043</transCode><channelFlow>20160409878999</channelFlow><serverFlow>OGW01201603161000008948</serverFlow><status>0</status><serverTime>175419</serverTime><errorCode>0</errorCode><errorMsg></errorMsg><serverDate>20160316</serverDate></header><body>< TRANSCODE>OGW00019</TRANSCODE ><MERCHANTID>XQB</ MERCHANTID><BANKID>GHB</BANKID><XMLPARA>7wPjJiSOm4uabTGPdh1Bnl81CzsONRae+NLuv3Zv1V7fa25nzxEgrJbXiYSCRmBn4XFahhEUzL33aSFWxoWlXisukbo+xPh4nRO/whIS5tP4AOkKjn0UvUqhmbS73Wj+nSNT5vNz6LFxNcPoRvoN2zokfuPQTo7ZoWzQZnhn+GQ=</XMLPARA></body></Document>";
    @Value("${des3Key}")
    private String des3Key;
    @Value("${merchantPrivateKey}")
    private String privateKey;
    @Value("${bankPublicKey}")
    private String publicKey;

    @Test
    public void test()
    {


        log.info(Utils.encrypt("<RETURN_STATUS>S</RETURN_STATUS><ERRORMSG></ERRORMSG><DFFLAG>1</DFFLAG><LOANNO>loanno</LOANNO><BWACNAME>朱晔</BWACNAME><BWACNO>aaa</BWACNO><TOTALNUM>2</TOTALNUM><RSLIST><SUBSEQNO>seq1</SUBSEQNO><ACNO>acno1</ACNO><ACNAME>acname1</ACNAME><INCOMEDATE>20180101</INCOMEDATE><AMOUNT>11</AMOUNT><STATUS>S</STATUS></RSLIST><RSLIST><SUBSEQNO>seq2</SUBSEQNO><ACNO>acno2</ACNO><ACNAME>acname2</ACNAME><INCOMEDATE>20180102</INCOMEDATE><AMOUNT>22</AMOUNT><STATUS>S</STATUS></RSLIST>", des3Key));
        //log.info(Utils.decrypt("L0t4anI5p51KuCJY+QO6djOw1WVv5QLEoZx3O3LEZdZGmfRu2Ypnip/S2588h8CH/HguG/zxe8dzds+RFnQFgnpDpKkEF2dvTMMijGpt6lgORCetd0ppLfIqe/40+8nrDCB8Ik0fcJ8AWDbregxabb8QRx76eZPrL0t4anI5p50hFMfPpdJ26oqUzGqhN3NPGbLnCO36sfopkP9s2Q6tSz5EwsdJBcHYJFtNmRqbup5SGVblh0t2S6mC+ugAHUJVYe2fFcKeCQaiTQV16IeDaqu9roxq6uxUb+pVi8OSenVNc2jWFIwur0Bnlsrf5HXTutK6mULqFTrysGM9wcn3z6DnPL78bTkxna8Zxr7dnWcPpsaWCgs/VE635PFZsLtbTg41bq3K/9EeFyiNb0Fm613z/zhl+bn5tjuwLzA1FfbEFUq4IxfX33p4mfh2j2VDUKPxM4i680BngFN9SfXmZQ==", des3Key));
        log.info(Utils.sign("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Document><header><channelCode>GHB</channelCode><channelFlow>OGW0120160602yL3dQM</channelFlow><channelDate>20160602</channelDate><channelTime>163827</channelTime><encryptData></encryptData><status>0</status></header><body><MERCHANTID>XJP</MERCHANTID><BANKID>GHB</BANKID><TRANSCODE>OGW00075</TRANSCODE><XMLPARA>n5/fD8rnDafoDoEeOPiwEqKYM9vNLUegdBJAtE8czfPKz2fqqFzB+31BpPVBlOHJ7dhbQVt52kRbHSjFM5nxe5q2eSqGBWja0dK+Nv78V1A0cBZMChFDA7HdmL8nNDGjCpAwU2RysBTLQaMKax8sXRBmf/WBJjdx0Yk0DdtmBy4aLcfGY4tzwl8ANIWBuT2hHRhcGr0Iiee2K37FUJw/tuAECfQV3d8CBdPjOC+yaIyK5ut1mxEBXp6ywYONuB57OgoNXOA/HZAcg7hkA2TX5q6bIpzjQSB5DLREtqIB49APRzK/n1ikDYXn6zOGe7Ocpft8lWgntE0tW0kJzVvQArdtF9YQ4jKYDvzfk0V18G8zZYV8fGju0beSpKJYPrzj++2AjmjekH6pe461fnTGBaW34JbpEflxaUVfbVuTNEEzxBEo62qSzkPRmgnfEyLdlvV09KDLeeqOESSyyvFonzncU8nL9GqP+vBu9mXvVhy95OJi4slQQVmcE4ijxOGAhrVGvyjZH9Hd2ih+0qU5ggEHfOi2eAJc7hd1Ix0/U8lOM7iXrIYHLNB1UStLLJ4CDfsWhVoFElZ/7weysLgyqsEm9NQSCQ4PdsofKLQ6GmfvlmEuGkA+3g==</XMLPARA></body></Document>", privateKey));
    }

    @Test
    public void generateKeys()
    {
        try
        {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(1024);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
            log.info("bankPublicKey:" + new BASE64Encoder().encodeBuffer(publicKey.getEncoded()));
            log.info("merchantPrivateKey:" + new BASE64Encoder().encodeBuffer(privateKey.getEncoded()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Test
    public void encrypt()
    {
        String data = "<BATCH_ID>580116</BATCH_ID><PROJECT_CODE>PRJ201603160236168741</PROJECT_CODE><PROJECT_ID>b904af2c-1834-434b-a0cb-1b91a8c19caa</PROJECT_ID>";
        String actual = Utils.encrypt(data, des3Key);
        String expected = "NVnwXP2rp6vOBT0MUM7ksYWuVU9JH/yK6Emtf8BLHTgMyyBjG2uy0nEzEJ40ch0dk0Q0bjvGw8/EFp7ccofRz07f5/gYJZq4Dwj7d+X8vyg2hnjyLfTJDCoDsBZ26ypULjYyPO5EjOLEUPTVbwX3/4L49nXtIStjYk6RgCDoV6LUjr3lJgOt+EkETdLRiYb0";
        log.info(actual);
        log.info(expected);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void decrypt()
    {
        String data = "NVnwXP2rp6vOBT0MUM7ksYWuVU9JH/yK6Emtf8BLHTgMyyBjG2uy0nEzEJ40ch0dk0Q0bjvGw8/EFp7ccofRz07f5/gYJZq4Dwj7d+X8vyg2hnjyLfTJDCoDsBZ26ypULjYyPO5EjOLEUPTVbwX3/4L49nXtIStjYk6RgCDoV6LUjr3lJgOt+EkETdLRiYb0";
        String actual = Utils.decrypt(data, des3Key);
        String expected = "<BATCH_ID>580116</BATCH_ID><PROJECT_CODE>PRJ201603160236168741</PROJECT_CODE><PROJECT_ID>b904af2c-1834-434b-a0cb-1b91a8c19caa</PROJECT_ID>";
        log.info(actual);
        log.info(expected);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sign()
    {
        String actual = Utils.sign(data, privateKey);
        String expected = "1D91E9936470396FD677736537A14E274E8184F80715A71BC48FC4478B2B73FA9B7ADF25FDCEEEA6D5CC3CB9CB88C1071118A70BEE1CF231138B25D0CFCE137F6BD22AB3F1C0A005F91F354D20EB8B10881C5F778F6956B72518622E2823ABAB46D5B4CBFBD163B87981E45A0D0E196EE4150C2F9CF143A6CC7FB2F77CB605F0";

        log.info(actual);
        log.info(expected);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void verifySign()
    {
        String sign = "1D91E9936470396FD677736537A14E274E8184F80715A71BC48FC4478B2B73FA9B7ADF25FDCEEEA6D5CC3CB9CB88C1071118A70BEE1CF231138B25D0CFCE137F6BD22AB3F1C0A005F91F354D20EB8B10881C5F778F6956B72518622E2823ABAB46D5B4CBFBD163B87981E45A0D0E196EE4150C2F9CF143A6CC7FB2F77CB605F0";
        Assert.assertTrue(Utils.verifySign(data, publicKey, sign));
    }

    @Test
    public void xml() throws IOException, SAXException, ParserConfigurationException
    {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Document><header><channelCode>GHB</channelCode><channelFlow>OGW0120160602yL3dQM</channelFlow><channelDate>20160602</channelDate><channelTime>163827</channelTime><encryptData></encryptData></header><body><MERCHANTID>XJP</MERCHANTID><BANKID>GHB</BANKID><TRANSCODE>OGWR0001</TRANSCODE><XMLPARA><OLDREQSEQNO>P2P0012016062304202HyMS9O</OLDREQSEQNO><ACNAME>张晓</ACNAME><IDTYPE>1010</IDTYPE><IDNO>4401**********0044</IDNO><MOBILE>134****0587</MOBILE><ACNO>6236882280000414248</ACNO></XMLPARA></body></Document>";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(xml)));
        log.info("header:" + $(document).find("header"));
        log.info("BANKID:" + $(document).find("body").find("BANKID").text());

    }
}
