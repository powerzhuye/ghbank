package com.kongzhong.finance.ghbank.service;

import com.kongzhong.finance.ghbank.api.ISyncService;
import com.kongzhong.finance.ghbank.api.ServiceException;
import com.kongzhong.finance.ghbank.api.enums.SendVerificationCodeOperationType;
import com.kongzhong.finance.ghbank.api.enums.SubmitRepayInvestProductIncomeRepayType;
import com.kongzhong.finance.ghbank.api.params.*;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuye on 21/01/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@SuppressWarnings("SpringJavaAutowiringInspection")
@Log
public class SyncServiceTest
{
    @Autowired
    private ISyncService syncService;

    @Test
    public void sendVerificationCodeTest() throws ServiceException
    {
        SendVerificationCodeParam param = new SendVerificationCodeParam();
        param.setAccountNo("aaa");
        param.setMobileNo("13651657101");
        param.setOperationType(SendVerificationCodeOperationType.自动投标撤销);
        log.info(syncService.sendVerificationCode(param).toString());
    }

    @Test
    public void checkCreateAccountResult() throws ServiceException
    {
        CheckCreateAccountResultParam param = new CheckCreateAccountResultParam();
        param.setOldRequestNo("P2P0922017031004200000001971");
        log.info(syncService.checkCreateAccountResult(param).toString());
    }

    @Test
    public void createInvestProduct() throws ServiceException
    {
        CreateInvestProductParam param = new CreateInvestProductParam();
        param.setInvestBeginDate(LocalDate.now());
        param.setInvestEndDate(LocalDate.now().plusDays(30));
        param.setInvestRangeDays(30);
        param.setInvestTotalAmount(new BigDecimal("10000"));
        param.setLoanNo("ZYTEST011");
        param.setMaxInvestAmount(new BigDecimal("10000"));
        param.setMinInvestAmount(new BigDecimal("100"));
        param.setProductName("朱晔测试");
        param.setProductDesc("朱晔测试描述");
        param.setRateType("setRateType");
        param.setRepayType("setRepayType");
        param.setRepayDate(LocalDate.now().plusDays(30));
        param.setRemark("setRemark");
        param.setYearRate(new BigDecimal("0.10"));
        param.setOldRequestSequence("");
        param.setRefLoanNo("");
        param.setTransfer(false);

        CreateInvestProductBorrowerParam borrower = new CreateInvestProductBorrowerParam();
        borrower.setAccountName("毛启");
        borrower.setAccountNo("6236882280000093190");
        borrower.setAmount(new BigDecimal("10000"));
        borrower.setBankId("123");
        borrower.setBankName("银行");
        borrower.setIdNo("230522197810224752");
        borrower.setCheckDate(LocalDate.now());
        borrower.setRemark("");
        borrower.setMortgageId("");
        borrower.setMortgageDesc("");
        param.setBorrower(borrower);
        log.info(syncService.createInvestProduct(param).toString());

    }

    @Test
    public void submitRepayInvestProductIncome() throws ServiceException
    {
        SubmitRepayInvestProductIncomeParam param = new SubmitRepayInvestProductIncomeParam();
        param.setOldRequestNo("P2P0922017031406700000002031");
        param.setBwAccountName("毛启");
        param.setBwAccountNo("6236882280000093190");
        param.setLoanNo("ZYTEST010");
        param.setTotalCount(1);
        param.setRepayType(SubmitRepayInvestProductIncomeRepayType.正常还款);
        List<SubmitRepayInvestProductIncomeItemParam> repayItems = new ArrayList<>();
        SubmitRepayInvestProductIncomeItemParam item1 = new SubmitRepayInvestProductIncomeItemParam();
        item1.setAccountName("蒋丽");
        item1.setAccountNo("6236882280000093133");
        item1.setAmount(new BigDecimal("11000"));
        item1.setFeeAmount(new BigDecimal("0"));
        item1.setIncomeAmount(new BigDecimal("1000"));
        item1.setPrincipalAmount(new BigDecimal("10000"));
        item1.setSubSequenceNo("P2P09220170314067000000020311");
        item1.setIncomeDate(LocalDate.now());
        repayItems.add(item1);
//        SubmitRepayInvestProductIncomeItemParam item2 = new SubmitRepayInvestProductIncomeItemParam();
//        item2.setAccountName("常又青");
//        item2.setAccountNo("6236882280000093117");
//        item2.setAmount(new BigDecimal("24000"));
//        item2.setFeeAmount(new BigDecimal("2000"));
//        item2.setIncomeAmount(new BigDecimal("2000"));
//        item2.setPrincipalAmount(new BigDecimal("20000"));
//        item2.setSubSequenceNo("P2P09220170309067000000019072");
//        item2.setIncomeDate(LocalDate.now());
//        repayItems.add(item2);
        param.setItems(repayItems);
        log.info(syncService.submitRepayInvestProductIncome(param).toString());
    }

    @Test
    public void submitInvestProductDiscount() throws ServiceException
    {
        SubmitInvestProductDiscountParam param = new SubmitInvestProductDiscountParam();
        param.setBwAccountName("setBwAccountName");
        param.setBwAccountNo("setBwAccountNo");
        param.setLoanNo("setLoanNo");
        param.setTotalCount(2);
        param.setAmount(new BigDecimal("100"));
        List<SubmitInvestProductDiscountItemParam> items = new ArrayList<>();
        SubmitInvestProductDiscountItemParam item1 = new SubmitInvestProductDiscountItemParam();
        item1.setAccountName("setAccountName1");
        item1.setAccountNo("setAccountNo1");
        item1.setAmount(new BigDecimal("11"));
        item1.setSubSequenceNo("setSubSequenceNo1");
        item1.setOldRequestNo("setOldRequestNo1");
        item1.setRemark("remark1");
        items.add(item1);
        SubmitInvestProductDiscountItemParam item2 = new SubmitInvestProductDiscountItemParam();
        item2.setAccountName("setAccountName2");
        item2.setAccountNo("setAccountNo2");
        item2.setAmount(new BigDecimal("22"));
        item2.setSubSequenceNo("setSubSequenceNo2");
        item2.setOldRequestNo("setOldRequestNo2");
        item2.setRemark("remark2");
        items.add(item2);
        param.setItems(items);
        log.info(syncService.submitInvestProductDiscount(param).toString());
    }

    @Test
    public void cancelPurchaseInvestProduct() throws ServiceException
    {
        CancelPurchaseInvestProductParam param = new CancelPurchaseInvestProductParam();
        param.setAccountName("朱晔");
        param.setAccountNo("aaaa");
        param.setCancelReason("setCancelReason");
        param.setLoanNo("setLoanNo");
        param.setOldRequestNo("setOldRequestNo");
        log.info(syncService.cancelPurchaseInvestProduct(param).toString());

    }

    @Test
    public void loanInvestProduct() throws ServiceException
    {
        LoanInvestProductParam param = new LoanInvestProductParam();
        param.setBwAccountName("毛启");
        param.setBwAccountNo("6236882280000093190");
        param.setRemark("放款");
        param.setLoanNo("ZYTEST011");
        param.setAccountManageAmount(new BigDecimal("0"));
        param.setGuaranteeAmount(new BigDecimal("0"));
        log.info(syncService.loanInvestProduct(param).toString());
    }

    @Test
    public void companyRepayInvestProduct() throws ServiceException
    {
        CompanyRepayInvestProductParam param = new CompanyRepayInvestProductParam();
        param.setBwAccountName("朱晔");
        param.setBwAccountNo("aaaa");
        param.setRemark("setRemark");
        param.setLoanNo("setLoanNo");
        param.setAmount(new BigDecimal("1000"));
        param.setFeeAmount(new BigDecimal("10"));
        log.info(syncService.companyRepayInvestProduct(param).toString());
    }

    @Test
    public void discardInvestProduct() throws ServiceException
    {
        DiscardInvestProductParam param = new DiscardInvestProductParam();
        param.setCancelReason("系统错误");
        param.setLoanNo("ZYTEST002");
        log.info(syncService.discardInvestProduct(param).toString());
    }

    @Test
    public void giveBonus() throws ServiceException
    {
        GiveBonusParam param = new GiveBonusParam();
        param.setAccountName("昌天磊");
        param.setAccountNo("6236882280000102975");
        param.setAmount(new BigDecimal("0.01"));
        param.setRemark("测试");
        log.info(syncService.giveBonus(param).toString());
    }

    @Test
    public void queryAccountBalance1() throws ServiceException
    {
        QueryAccountBalanceParam param = new QueryAccountBalanceParam();
        param.setAccountName("蒋丽");
        param.setAccountNo("6236882280000093133");
        log.info(syncService.queryAccountBalance(param).toString());
    }

    @Test
    public void queryAccountBalance2() throws ServiceException
    {
        QueryAccountBalanceParam param = new QueryAccountBalanceParam();
        param.setAccountName("毛启");
        param.setAccountNo("6236882280000093190");
        log.info(syncService.queryAccountBalance(param).toString());
    }

    @Test
    public void queryAccountBalance3() throws ServiceException
    {
        QueryAccountBalanceParam param = new QueryAccountBalanceParam();
        param.setAccountName("苗瑞丽");
        param.setAccountNo("6236882280000093109");
        log.info(syncService.queryAccountBalance(param).toString());
    }

    @Test
    public void checkPurchaseInvestProductResult() throws ServiceException
    {
        CheckPurchaseInvestProductResultParam param = new CheckPurchaseInvestProductResultParam();
        param.setOldRequestNo("P2P0922017031505200000002082");
        log.info(syncService.checkPurchaseInvestProductResult(param).toString());
    }

    @Test
    public void checkDailyTransactionResult() throws ServiceException
    {
        CheckDailyTransactionResultParam param = new CheckDailyTransactionResultParam();
        param.setCheckDate(LocalDate.of(2017, 02, 28));
        log.info(syncService.checkDailyTransactionResult(param).toString());
    }

    @Test
    public void checkDepositAccountResult() throws ServiceException
    {
        CheckDepositAccountResultParam param = new CheckDepositAccountResultParam();
        param.setOldRequestNo("aa");
        log.info(syncService.checkDepositAccountResult(param).toString());
    }

    @Test
    public void checkWithdrawAccountResult() throws ServiceException
    {
        CheckWithdrawAccountResultParam param = new CheckWithdrawAccountResultParam();
        param.setOldRequestNo("aa");
        param.setTransactionDate(LocalDate.now());
        log.info(syncService.checkWithdrawAccountResult(param).toString());
    }

    @Test
    public void checkLoanInvestProductResult() throws ServiceException
    {
        CheckLoanInvestProductResultParam param = new CheckLoanInvestProductResultParam();
        param.setLoanNo("ZYTEST011");
        param.setOldPurchaseProductNo("");
        param.setOldRequestNo("P2P0922017031506500000002086");
        log.info(syncService.checkLoanInvestProductResult(param).toString());
    }

    @Test
    public void checkDiscardInvestProductResult() throws ServiceException
    {
        CheckDiscardInvestProductResultParam param = new CheckDiscardInvestProductResultParam();
        param.setOldRequestNo("setOldRequestNo");
        param.setOldPurchaseProductNo("setOldPurchaseProductNo");
        log.info(syncService.checkDiscardInvestProductResult(param).toString());
    }

    @Test
    public void checkRepayInvestProductResult() throws ServiceException
    {
        CheckRepayInvestProductResultParam param = new CheckRepayInvestProductResultParam();
        param.setOldRequestNo("P2P0922017030906700000001907");
        log.info(syncService.checkRepayInvestProductResult(param).toString());
    }

    @Test
    public void checkAuthAutoRepayInvestProductResult() throws ServiceException
    {
        CheckAuthAutoRepayInvestProductResultParam param = new CheckAuthAutoRepayInvestProductResultParam();
        param.setOldRequestNo("setOldRequestNo");
        log.info(syncService.checkAuthAutoRepayInvestProductResult(param).toString());
    }

    @Test
    public void checkAuthAutoPurchaseInvestProductResult() throws ServiceException
    {
        CheckAuthAutoPurchaseInvestProductResultParam param = new CheckAuthAutoPurchaseInvestProductResultParam();
        param.setOldRequestNo("setOldRequestNo");
        log.info(syncService.checkAuthAutoPurchaseInvestProductResult(param).toString());
    }

    @Test
    public void checkTransferInvestProductResult() throws ServiceException
    {
        CheckTransferInvestProductResultParam param = new CheckTransferInvestProductResultParam();
        param.setOldRequestNo("setOldRequestNo");
        log.info(syncService.checkTransferInvestProductResult(param).toString());
    }

    @Test
    public void cancelAuthAutoPurchaseInvestProduct() throws ServiceException
    {
        CancelAuthAutoPurchaseInvestProductParam param = new CancelAuthAutoPurchaseInvestProductParam();
        param.setAccountName("朱晔");
        param.setAccountNo("111");
        param.setMessageNo("222");
        param.setMessageSequenceNo("33333");
        param.setRemark("setRemark");
        log.info(syncService.cancelAuthAutoPurchaseInvestProduct(param).toString());
    }

    @Test
    public void cancelAuthAutoRepayInvestProduct() throws ServiceException
    {
        CancelAuthAutoRepayInvestProductParam param = new CancelAuthAutoRepayInvestProductParam();
        param.setBwAccountName("朱晔");
        param.setBwAccountNo("111");
        param.setLoanNo("setLoanNo");
        param.setMessageNo("222");
        param.setMessageSequenceNo("33333");
        param.setRemark("setRemark");
        log.info(syncService.cancelAuthAutoRepayInvestProduct(param).toString());
    }

    @Test
    public void autoRepayInvestProduct() throws ServiceException
    {
        AutoRepayInvestProductParam param = new AutoRepayInvestProductParam();
        param.setBwAccountNo("111");
        param.setBwAccountName("朱晔");
        param.setAmount(new BigDecimal("10000"));
        param.setFeeAmount(new BigDecimal("100"));
        param.setLoanNo("setLoanNo");
        param.setRemark("setRemark");
        log.info(syncService.autoRepayInvestProduct(param).toString());
    }

    @Test
    public void autoPurchaseInvestProduct() throws ServiceException
    {
        AutoPurchaseInvestProductParam param = new AutoPurchaseInvestProductParam();
        param.setAccountNo("6236882280000093109");
        param.setAccountName("苗瑞丽");
        param.setAmount(new BigDecimal("10000"));
        param.setLoanNo("ZYTEST009");
        param.setRemark("test");
        log.info(syncService.autoPurchaseInvestProduct(param).toString());
    }

    @Test
    public void checkSubmitRepayInvestProductIncomeResult() throws ServiceException
    {
        CheckSubmitRepayInvestProductIncomeResultParam param = new CheckSubmitRepayInvestProductIncomeResultParam();
        param.setLoanNo("ZYTEST010");
        param.setOldRequestNo("P2P0922017031407400000002032");
        param.setSubSequenceNo("");
        log.info(syncService.checkSubmitRepayInvestProductIncomeResult(param).toString());
    }

    @Test
    public void checkSubmitInvestProductDiscountResult() throws ServiceException
    {
        CheckSubmitInvestProductDiscountResultParam param = new CheckSubmitInvestProductDiscountResultParam();
        param.setOldRequestNo("setOldRequestNo");
        param.setSubSequenceNo("setSubSequenceNo");
        log.info(syncService.checkSubmitInvestProductDiscountResult(param).toString());
    }
}
