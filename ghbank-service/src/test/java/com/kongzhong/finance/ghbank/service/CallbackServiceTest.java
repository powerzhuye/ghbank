package com.kongzhong.finance.ghbank.service;

import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zhuye on 21/01/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@SuppressWarnings("SpringJavaAutowiringInspection")
@Log
public class CallbackServiceTest
{
    @Autowired
    private CallbackService callbackService;

    @Test
    public void createAccountCallback()
    {
        String data = "001X11          00000256C0D8514BD7C44B500B669EFB8663853E466A1D71EE3B60AEC6D4A48EFC64D1DA2050323F08CB232D76EDD6D1C3DE4EF159BE5758A1F88A0C82F912027EACCBB9C4DC87467F3EBC509988B57466B6E7B20B8CC865CE53357DF31EFB52694689B54E8CE9043139BB62B66559795C4EDD2E4E891B582986119CED353EF09A649B18<?xml version=\"1.0\" encoding=\"UTF-8\"?><Document><header><channelCode>GHB</channelCode><channelFlow>OGW0120170314EAY45g</channelFlow><channelDate>20170314</channelDate><channelTime>095946</channelTime><encryptData></encryptData></header><body><MERCHANTID>KZJR</MERCHANTID><BANKID>GHB</BANKID><TRANSCODE>OGWR0003</TRANSCODE><XMLPARA>kMIIhyVeHHAwBhqk+XsuskX6jnDP5kNo0ocKwOwDCeaDudYVYLJCco+Qs+0hP4BXOuX3wJF62Tn13iGTFEwr0ba8ImWdg0o6+DPgSVZhFa85lw0Ot/BeFy8WZ7e4kKUHaZGlptORSZ+w5hc6x11zfqGMC3D5cwgbRK/BybQWil86zU9TR5MaVjCySsC78ku/KGEUfVECDsk=</XMLPARA></body></Document>";
        log.info(callbackService.callback(data));
    }
}
