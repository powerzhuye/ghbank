package com.kongzhong.finance.ghbank.api.dtos;

import lombok.Data;

/**
 * Created by zhuye on 14/02/2017.
 */
@Data
public class AsyncServiceDto
{
    private String formHtml;
    private String transactionNo;
}
