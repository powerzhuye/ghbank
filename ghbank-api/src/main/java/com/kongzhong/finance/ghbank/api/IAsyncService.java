package com.kongzhong.finance.ghbank.api;

import com.kongzhong.finance.ghbank.api.dtos.AsyncServiceDto;
import com.kongzhong.finance.ghbank.api.params.*;

/**
 * Created by zhuye on 12/02/2017.
 */
public interface IAsyncService
{
    //账户开立(OGW00042) （必选，跳转我行页面处理）
    AsyncServiceDto createAccount(CreateAccountParam param);

    //绑卡(OGW00044)（可选，跳转我行页面处理）
    AsyncServiceDto bindCard(BindCardParam param);

    //单笔专属账户充值(OGW00045) （跳转我行页面处理）
    AsyncServiceDto depositAccount(DepositAccountParam param);

    //单笔提现(OGW00047) （跳转我行页面处理）
    AsyncServiceDto withdrawAccount(WithdrawAccountParam param);

    //单笔投标 (OGW00052)（跳转我行页面处理）
    AsyncServiceDto purchaseInvestProduct(PurchaseInvestProductParam param);

    //债券转让申请(OGW00061) （可选，跳转我行页面处理）
    AsyncServiceDto transferInvestProduct(TransferInvestProductParam param);

    //自动投标授权(OGW00056) （可选，跳转我行页面处理）
    AsyncServiceDto authAutoPurchaseInvestProduct(AuthAutoPurchaseInvestProductParam param);

    //借款人单标还款 (OGW00067) （必须，跳转我行页面处理）
    AsyncServiceDto repayInvestProduct(RepayInvestProductParam param);

    //自动还款授权 (OGW00069) （可选，跳转我行页面处理）
    AsyncServiceDto authAutoRepayInvestProduct(AuthAutoRepayInvestProductParam param);
}
