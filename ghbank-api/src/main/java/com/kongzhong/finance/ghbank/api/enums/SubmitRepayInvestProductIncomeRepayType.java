package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum SubmitRepayInvestProductIncomeRepayType
{
    测试(0),
    正常还款(1),
    垫付后借款人还款(5);

    @Getter
    private final int code;

    public static SubmitRepayInvestProductIncomeRepayType parse(Integer code)
    {
        return Arrays.stream(values()).filter(item -> item.code == (code == null ? 0 : code))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%d", SubmitRepayInvestProductIncomeRepayType.class.getName(), code)));
    }
}
