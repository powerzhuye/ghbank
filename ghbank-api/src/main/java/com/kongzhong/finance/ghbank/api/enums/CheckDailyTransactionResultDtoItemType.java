package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 11/02/2017.
 */
@RequiredArgsConstructor
public enum CheckDailyTransactionResultDtoItemType
{
    投标(1),
    撤标(2),
    流标(3),
    放款(4),
    还款(5),
    专属账户充值(20),
    公司垫付(21),
    投标优惠返回(22),
    单笔奖励或分红(23),
    提现(24),
    自动投标(30),
    自动还款(33);

    @Getter
    private final int code;

    public static CheckDailyTransactionResultDtoItemType parse(int code)
    {
        return Arrays.stream(values()).filter(item -> item.code == code)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%d", CheckDailyTransactionResultDtoItemType.class.getName(), code)));
    }
}
