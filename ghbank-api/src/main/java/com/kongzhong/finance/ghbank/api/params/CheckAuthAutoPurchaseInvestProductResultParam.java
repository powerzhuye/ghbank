package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class CheckAuthAutoPurchaseInvestProductResultParam
{
    private String oldRequestNo;
}
