package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckCreateAccountResultStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
@Builder
public class CheckCreateAccountResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private CheckCreateAccountResultStatus status;
    private String transactionSequenceNo;
    private LocalDateTime transactionDatetime;
    private String accountNo;
    private String accountName;
    private String idType;
    private String idNo;
    private String mobile;
}
