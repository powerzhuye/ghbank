package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class AutoRepayInvestProductParam
{
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private String remark;
    private BigDecimal amount;
    private BigDecimal feeAmount;
}
