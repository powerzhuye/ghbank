package com.kongzhong.finance.ghbank.api;

import lombok.Data;

/**
 * Created by zhuye on 12/02/2017.
 */
@Data
public class ServiceException extends Exception
{
    private final String errorCode;
    private final String errorMessage;

    public ServiceException(String errorCode, String errorMessage)
    {
        super(errorMessage);

        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}

