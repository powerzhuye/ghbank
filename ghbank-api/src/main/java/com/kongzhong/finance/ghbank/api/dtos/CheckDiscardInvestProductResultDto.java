package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckDiscardInvestProductResultStatus;
import lombok.Data;

import java.util.List;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CheckDiscardInvestProductResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private CheckDiscardInvestProductResultStatus status;
    private List<CheckDiscardInvestProductResultItemDto> items;
}
