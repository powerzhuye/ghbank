package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum SendVerificationCodeOperationType
{
    默认(0),
    自动投标撤销(1),
    自动还款授权撤销(2);

    @Getter
    private final int code;

    public static SendVerificationCodeOperationType parse(int code)
    {
        return Arrays.stream(values()).filter(item -> item.code == code)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%d", SendVerificationCodeOperationType.class.getName(), code)));
    }
}


