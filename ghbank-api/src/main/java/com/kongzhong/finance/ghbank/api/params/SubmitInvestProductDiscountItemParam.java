package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
public class SubmitInvestProductDiscountItemParam
{
    private String accountNo;
    private String accountName;
    private BigDecimal amount;
    private String subSequenceNo;
    private String oldRequestNo;
    private String remark;
}
