package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
public class SubmitRepayInvestProductIncomeItemParam
{
    private String subSequenceNo;
    private String accountNo;
    private String accountName;
    private LocalDate incomeDate;
    private BigDecimal amount;
    private BigDecimal principalAmount;
    private BigDecimal incomeAmount;
    private BigDecimal feeAmount;
}
