package com.kongzhong.finance.ghbank.api.params;

import com.kongzhong.finance.ghbank.api.enums.SubmitRepayInvestProductIncomeRepayType;
import lombok.Data;

import java.util.List;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
public class SubmitRepayInvestProductIncomeParam
{
    private String oldRequestNo;
    private SubmitRepayInvestProductIncomeRepayType repayType;
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private Integer totalCount;
    private List<SubmitRepayInvestProductIncomeItemParam> items;

}
