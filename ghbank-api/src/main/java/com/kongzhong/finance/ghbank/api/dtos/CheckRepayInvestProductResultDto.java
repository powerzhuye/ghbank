package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckRepayInvestProductResultStatus;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
@Builder
public class CheckRepayInvestProductResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private CheckRepayInvestProductResultStatus status;
    private String transactionSequenceNo;
    private LocalDateTime transactionDatetime;
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private BigDecimal amount;
}
