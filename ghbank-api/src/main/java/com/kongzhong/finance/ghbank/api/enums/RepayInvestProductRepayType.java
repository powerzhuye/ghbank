package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 10/02/2017.
 */
@RequiredArgsConstructor
public enum RepayInvestProductRepayType
{
    正常还款(1),
    垫付后还款(2);

    @Getter
    private final int code;

    public static RepayInvestProductRepayType parse(int code)
    {
        return Arrays.stream(values()).filter(item -> item.code == code)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%d", RepayInvestProductRepayType.class.getName(), code)));
    }
}
