package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class AuthAutoRepayInvestProductParam
{
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private String remark;
    private String returnUrl;
}
