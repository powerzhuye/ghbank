package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum CheckDailyTransactionResultStatus
{
    测试(""),
    正在处理中("L"),
    不可对账("F"),
    可对账("S");

    @Getter
    private final String code;

    public static CheckDailyTransactionResultStatus parse(String code)
    {
        return Arrays.stream(values()).filter(item -> item.code.equals(code == null ? "" : code))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%s", CheckDailyTransactionResultStatus.class.getName(), code)));
    }
}


