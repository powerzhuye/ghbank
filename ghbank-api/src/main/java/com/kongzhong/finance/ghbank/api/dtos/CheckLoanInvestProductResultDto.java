package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckLoanInvestProductResultStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CheckLoanInvestProductResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private BigDecimal accountManageAmount;
    private BigDecimal guaranteeAmount;
    private CheckLoanInvestProductResultStatus status;
    private List<CheckLoanInvestProductResultItemDto> items;
}
