package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class AutoPurchaseInvestProductParam
{
    private String loanNo;
    private String accountName;
    private String accountNo;
    private String remark;
    private BigDecimal amount;
}
