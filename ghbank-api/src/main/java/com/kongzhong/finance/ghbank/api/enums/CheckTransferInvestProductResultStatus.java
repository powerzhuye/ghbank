package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum CheckTransferInvestProductResultStatus
{
    测试(""),
    成功("S"),
    失败("F"),
    处理中("R"),
    未知("N");

    @Getter
    private final String code;

    public static CheckTransferInvestProductResultStatus parse(String code)
    {
        return Arrays.stream(values()).filter(item -> item.code.equals(code == null ? "" : code))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%s", CheckTransferInvestProductResultStatus.class.getName(), code)));
    }
}


