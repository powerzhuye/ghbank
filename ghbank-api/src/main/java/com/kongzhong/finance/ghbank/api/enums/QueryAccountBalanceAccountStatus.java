package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum QueryAccountBalanceAccountStatus
{
    测试(""),
    未激活("0"),
    正常("1"),
    异常("A"),
    销户("3");

    @Getter
    private final String code;

    public static QueryAccountBalanceAccountStatus parse(String code)
    {
        return Arrays.stream(values()).filter(item -> item.code.equals(code == null ? "" : code))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%s", QueryAccountBalanceAccountStatus.class.getName(), code)));
    }
}
