package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CreateInvestProductBorrowerParam
{
    private String accountName;
    private String idNo;
    private String accountNo;
    private String bankId;
    private String bankName;
    private BigDecimal amount;
    private String mortgageId;
    private String mortgageDesc;
    private LocalDate checkDate;
    private String remark;
}
