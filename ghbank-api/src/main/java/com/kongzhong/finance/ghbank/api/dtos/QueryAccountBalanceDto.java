package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.QueryAccountBalanceAccountStatus;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
@Builder
public class QueryAccountBalanceDto
{
    private ResultHeader header;
    private String accountNo;
    private String accountName;
    private BigDecimal accountBalance;
    private BigDecimal availableBalance;
    private BigDecimal frozenBalance;
    private QueryAccountBalanceAccountStatus accountStatus;
}
