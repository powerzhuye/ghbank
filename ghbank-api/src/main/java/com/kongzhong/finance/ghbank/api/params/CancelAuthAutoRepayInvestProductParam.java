package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CancelAuthAutoRepayInvestProductParam
{
    private String messageSequenceNo;
    private String messageNo;
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private String remark;
}
