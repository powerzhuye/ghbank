package com.kongzhong.finance.ghbank.api;

/**
 * Created by zhuye on 14/02/2017.
 */
public interface ICallbackService
{
    String callback(String data);
}
