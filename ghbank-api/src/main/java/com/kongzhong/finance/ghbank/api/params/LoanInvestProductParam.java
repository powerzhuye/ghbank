package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class LoanInvestProductParam
{
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private BigDecimal accountManageAmount;
    private BigDecimal guaranteeAmount;
    private String remark;
}
