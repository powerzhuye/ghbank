package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class TransferInvestProductParam
{
    private String returnUrl;
    private String oldPurchaseRequestNo;
    private String oldProductId;
    private String oldProductName;
    private String accountNo;
    private String accountName;
    private BigDecimal predictIncome;
    private BigDecimal amount;
    private String remark;
}
