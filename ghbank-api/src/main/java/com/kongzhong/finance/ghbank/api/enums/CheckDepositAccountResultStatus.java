package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum CheckDepositAccountResultStatus
{
    测试(""),
    页面处理中("R"),
    未知("N"),
    预授权成功("P"),
    后台支付系统处理中("D"),
    成功("S"),
    失败("F");

    @Getter
    private final String code;

    public static CheckDepositAccountResultStatus parse(String code)
    {
        return Arrays.stream(values()).filter(item -> item.code.equals(code == null ? "" : code))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%s", CheckDepositAccountResultStatus.class.getName(), code)));
    }

}


