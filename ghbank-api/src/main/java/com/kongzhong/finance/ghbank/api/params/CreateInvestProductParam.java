package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CreateInvestProductParam
{
    private String loanNo;
    private String productName;
    private String productDesc;
    private BigDecimal minInvestAmount;
    private BigDecimal maxInvestAmount;
    private BigDecimal investTotalAmount;
    private LocalDate investBeginDate;
    private LocalDate investEndDate;
    private LocalDate repayDate;
    private BigDecimal yearRate;
    private Integer investRangeDays;
    private String rateType;
    private String repayType;
    private String remark;
    private boolean isTransfer = false;
    private String refLoanNo;
    private String oldRequestSequence;
    private CreateInvestProductBorrowerParam borrower;
}
