package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckSubmitInvestProductDiscountResultStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CheckSubmitInvestProductDiscountResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String transactionSequenceNo;
    private LocalDateTime transactionDatetime;
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private BigDecimal amount;
    private Integer totalCount;
    private CheckSubmitInvestProductDiscountResultStatus status;

    private List<CheckSubmitInvestProductDiscountResultItemDto> items;
}
