package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class BindCardParam
{
    private String returnUrl;
    private String accountNo;
}
