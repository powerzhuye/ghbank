package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class CreateAccountParam
{
    private String returnUrl;
    private String accountName;
    private String idNo;
    private String mobile;
    private String email;
    private String customerManagerNo;
}
