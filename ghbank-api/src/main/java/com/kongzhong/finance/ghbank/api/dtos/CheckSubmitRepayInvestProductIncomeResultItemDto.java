package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckSubmitRepayInvestProductIncomeResultItemStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by zhuye on 12/02/2017.
 */
@Data
public class CheckSubmitRepayInvestProductIncomeResultItemDto
{
    private String subSequenceNo;
    private String accountNo;
    private String accountName;
    private LocalDate incomeDate;
    private BigDecimal amount;
    private BigDecimal principalAmount;
    private BigDecimal incomeAmount;
    private BigDecimal feeAmount;
    private String errorMessage;
    private String bankTransactionNo;
    private LocalDate transactionDate;
    private CheckSubmitRepayInvestProductIncomeResultItemStatus status;

}
