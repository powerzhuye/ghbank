package com.kongzhong.finance.ghbank.api.dtos;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
@Builder
public class SubmitInvestProductDiscountDto
{
    private ResultHeader header;
    private String transactionSequenceNo;
    private LocalDateTime transactionDatetime;
}
