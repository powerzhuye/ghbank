package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class CancelPurchaseInvestProductParam
{
    private String loanNo;
    private String oldRequestNo;
    private String accountNo;
    private String accountName;
    private String cancelReason;
}
