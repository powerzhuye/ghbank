package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.time.LocalDate;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class CheckDailyTransactionResultParam
{
    private LocalDate checkDate;
}
