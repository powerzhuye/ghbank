package com.kongzhong.finance.ghbank.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum CheckDiscardInvestProductResultStatus
{
    测试(""),
    交易处理中("L"),
    成功("S"),
    失败("F");

    @Getter
    private final String code;

    public static CheckDiscardInvestProductResultStatus parse(String code)
    {
        return Arrays.stream(values()).filter(item -> item.code.equals(code == null ? "" : code))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%s", CheckDiscardInvestProductResultStatus.class.getName(), code)));
    }
}


