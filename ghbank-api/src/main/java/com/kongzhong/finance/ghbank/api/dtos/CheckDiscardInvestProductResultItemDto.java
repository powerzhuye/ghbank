package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckDiscardInvestProductResultItemStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by zhuye on 12/02/2017.
 */
@Data
public class CheckDiscardInvestProductResultItemDto
{
    private String requestNo;
    private String loanNo;
    private String accountNo;
    private String accountName;
    private BigDecimal amount;
    private LocalDate transactionDate;
    private CheckDiscardInvestProductResultItemStatus status;
    private String errorMessage;
    private String bankTransactionNo;
}
