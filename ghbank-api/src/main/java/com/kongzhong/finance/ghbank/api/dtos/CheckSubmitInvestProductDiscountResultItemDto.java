package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckSubmitInvestProductDiscountResultItemStatus;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 12/02/2017.
 */
@Data
public class CheckSubmitInvestProductDiscountResultItemDto
{
    private CheckSubmitInvestProductDiscountResultItemStatus status;
    private String subSequenceNo;
    private String oldRequestNo;
    private String accountNo;
    private String accountName;
    private BigDecimal amount;
    private String remark;
    private String errorMessage;
}
