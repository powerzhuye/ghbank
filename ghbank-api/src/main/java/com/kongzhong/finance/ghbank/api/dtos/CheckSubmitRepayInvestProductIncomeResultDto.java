package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckSubmitRepayInvestProductIncomeResultStatus;
import com.kongzhong.finance.ghbank.api.enums.SubmitRepayInvestProductIncomeRepayType;
import lombok.Data;

import java.util.List;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CheckSubmitRepayInvestProductIncomeResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private SubmitRepayInvestProductIncomeRepayType repayType;
    private String loanNo;
    private String bwAccountNo;
    private String bwAccountName;
    private Integer totalCount;
    private CheckSubmitRepayInvestProductIncomeResultStatus status;
    private List<CheckSubmitRepayInvestProductIncomeResultItemDto> items;
}
