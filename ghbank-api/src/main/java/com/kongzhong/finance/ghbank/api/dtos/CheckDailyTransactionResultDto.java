package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckDailyTransactionResultStatus;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CheckDailyTransactionResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private CheckDailyTransactionResultStatus status;
    private String fileName;
    private String merchantNo;
    private LocalDate checkDate;
    private Integer transactionCount;
    private List<CheckDailyTransactionResultItemDto> items;

}
