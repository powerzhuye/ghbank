package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckDailyTransactionResultDtoItemType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CheckDailyTransactionResultItemDto
{
    private CheckDailyTransactionResultDtoItemType type;
    private String parentTransactionNo;
    private String childTransactionNo;
    private String loanNo;
    private String payAccountNo;
    private String payAccountName;
    private String payAccountBankId;
    private String payAccountBankName;
    private String receiveAccountNo;
    private String receiveAccountName;
    private String receiveAccountBankId;
    private String receiveAccountBankName;
    private BigDecimal amount;
    private BigDecimal feeAmount;
    private BigDecimal managementAmount;
    private BigDecimal guaranteeAmount;
    private LocalDate bankSettleDate;
    private String bankSettleTransactionNo;
}
