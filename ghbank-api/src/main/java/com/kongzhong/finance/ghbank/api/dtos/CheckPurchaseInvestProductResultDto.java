package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckPurchaseInvestProductResultStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
@Builder
public class CheckPurchaseInvestProductResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private String transactionSequenceNo;
    private LocalDateTime transactionDatetime;
    private CheckPurchaseInvestProductResultStatus status;
}
