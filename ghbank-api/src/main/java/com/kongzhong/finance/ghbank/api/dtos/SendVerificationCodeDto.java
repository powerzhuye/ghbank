package com.kongzhong.finance.ghbank.api.dtos;

import lombok.Builder;
import lombok.Data;

/**
 * Created by zhuye on 18/01/2017.
 */
@Builder
@Data
public class SendVerificationCodeDto
{
    private ResultHeader header;
    private String messageSequenceNo;
    private String messageIndex;
}
