package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class QueryAccountBalanceParam
{
    private String accountNo;
    private String accountName;
}
