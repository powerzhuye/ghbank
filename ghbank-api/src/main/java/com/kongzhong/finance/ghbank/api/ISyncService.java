package com.kongzhong.finance.ghbank.api;

import com.kongzhong.finance.ghbank.api.dtos.*;
import com.kongzhong.finance.ghbank.api.params.*;

/**
 * Created by zhuye on 12/02/2017.
 */
public interface ISyncService
{
    //获取短信验证码(OGW00041)
    SendVerificationCodeDto sendVerificationCode(SendVerificationCodeParam param) throws ServiceException;

    //投标优惠返回（可选）(OGW00054)
    SubmitInvestProductDiscountDto submitInvestProductDiscount(SubmitInvestProductDiscountParam param) throws ServiceException;

    //投标优惠返回结果查询（可选）(OGW00055)
    CheckSubmitInvestProductDiscountResultDto checkSubmitInvestProductDiscountResult(CheckSubmitInvestProductDiscountResultParam param) throws ServiceException;

    //单笔发标信息通知(OGW00051)
    CreateInvestProductDto createInvestProduct(CreateInvestProductParam param) throws ServiceException;

    //单笔撤标(OGW00060)
    CancelPurchaseInvestProductDto cancelPurchaseInvestProduct(CancelPurchaseInvestProductParam param) throws ServiceException;

    //放款(OGW00065)
    LoanInvestProductDto loanInvestProduct(LoanInvestProductParam param) throws ServiceException;

    //流标(OGW00063)
    DiscardInvestProductDto discardInvestProduct(DiscardInvestProductParam param) throws ServiceException;

    //账户余额查询(OGW00049)
    QueryAccountBalanceDto queryAccountBalance(QueryAccountBalanceParam param) throws ServiceException;

    //单笔充值结果查询 (OGW00046)
    CheckDepositAccountResultDto checkDepositAccountResult(CheckDepositAccountResultParam param) throws ServiceException;

    //单笔提现结果查询(OGW00048)
    CheckWithdrawAccountResultDto checkWithdrawAccountResult(CheckWithdrawAccountResultParam param) throws ServiceException;

    //放款结果查询 (OGW00066)
    CheckLoanInvestProductResultDto checkLoanInvestProductResult(CheckLoanInvestProductResultParam param) throws ServiceException;

    //流标结果查询 (OGW00064)
    CheckDiscardInvestProductResultDto checkDiscardInvestProductResult(CheckDiscardInvestProductResultParam param) throws ServiceException;

    //借款人单标还款结果查询(OGW00068)
    CheckRepayInvestProductResultDto checkRepayInvestProductResult(CheckRepayInvestProductResultParam param) throws ServiceException;

    //自动还款授权结果查询（可选）(OGW00070)
    CheckAuthAutoRepayInvestProductResultDto checkAuthAutoRepayInvestProductResult(CheckAuthAutoRepayInvestProductResultParam param) throws ServiceException;

    //自动投标授权结果查询（可选）(OGW00057)
    CheckAuthAutoPurchaseInvestProductDto checkAuthAutoPurchaseInvestProductResult(CheckAuthAutoPurchaseInvestProductResultParam param) throws ServiceException;

    //自动投标授权撤销（可选）(OGW00058)
    CancelAuthAutoPurchaseInvestProductDto cancelAuthAutoPurchaseInvestProduct(CancelAuthAutoPurchaseInvestProductParam param) throws ServiceException;

    //自动还款授权撤销（可选）(OGW00071)
    CancelAuthAutoRepayInvestProductDto cancelAuthAutoRepayInvestProduct(CancelAuthAutoRepayInvestProductParam param) throws ServiceException;

    //账户开立结果查询(OGW00043)
    CheckCreateAccountResultDto checkCreateAccountResult(CheckCreateAccountResultParam param) throws ServiceException;

    //单笔投标结果查询(OGW00053)
    CheckPurchaseInvestProductResultDto checkPurchaseInvestProductResult(CheckPurchaseInvestProductResultParam param) throws ServiceException;

    //债权转让结果查询(OGW00062)
    CheckTransferInvestProductResultDto checkTransferInvestProductResult(CheckTransferInvestProductResultParam param) throws ServiceException;

    //自动单笔还款（可选）(OGW00072)
    AutoRepayInvestProductDto autoRepayInvestProduct(AutoRepayInvestProductParam param) throws ServiceException;

    //自动单笔投标（可选）(OGW00059)
    AutoPurchaseInvestProductDto autoPurchaseInvestProduct(AutoPurchaseInvestProductParam param) throws ServiceException;

    //单标公司垫付还款(OGW00073)
    CompanyRepayInvestProductDto companyRepayInvestProduct(CompanyRepayInvestProductParam param) throws ServiceException;

    //还款收益明细提交(OGW00074)
    SubmitRepayInvestProductIncomeDto submitRepayInvestProductIncome(SubmitRepayInvestProductIncomeParam param) throws ServiceException;

    //还款收益结果查询 (OGW00075)
    CheckSubmitRepayInvestProductIncomeResultDto checkSubmitRepayInvestProductIncomeResult(CheckSubmitRepayInvestProductIncomeResultParam param) throws ServiceException;

    //单笔奖励或分红（可选）(OGW00076)
    GiveBonusDto giveBonus(GiveBonusParam param) throws ServiceException;

    //日终对账请求(OGW00077)
    CheckDailyTransactionResultDto checkDailyTransactionResult(CheckDailyTransactionResultParam param) throws ServiceException;
}
