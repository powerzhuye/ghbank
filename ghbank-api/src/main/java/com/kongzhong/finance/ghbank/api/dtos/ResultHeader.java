package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.ResultStatus;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by zhuye on 12/02/2017.
 */
@Data
public class ResultHeader
{
    private String serverFlow;
    private LocalDateTime serverTime;
    private ResultStatus status;
}
