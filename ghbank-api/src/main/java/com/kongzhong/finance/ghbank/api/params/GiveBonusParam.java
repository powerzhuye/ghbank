package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class GiveBonusParam
{
    private String accountNo;
    private String accountName;
    private BigDecimal amount;
    private String remark;
}
