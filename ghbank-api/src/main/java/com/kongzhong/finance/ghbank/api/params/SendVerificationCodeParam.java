package com.kongzhong.finance.ghbank.api.params;

import com.kongzhong.finance.ghbank.api.enums.SendVerificationCodeOperationType;
import lombok.Data;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class SendVerificationCodeParam
{
    private SendVerificationCodeOperationType operationType;
    private String accountNo;
    private String mobileNo;
}
