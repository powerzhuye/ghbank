package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class PurchaseInvestProductParam
{
    private String loanNo;
    private String returnUrl;
    private String accountNo;
    private String accountName;
    private BigDecimal amount;
    private String remark;

}
