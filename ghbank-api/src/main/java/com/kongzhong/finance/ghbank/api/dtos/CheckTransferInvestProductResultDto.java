package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckTransferInvestProductResultStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
@Builder
public class CheckTransferInvestProductResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private CheckTransferInvestProductResultStatus status;
    private String transactionSequenceNo;
    private LocalDateTime transactionDatetime;
}
