package com.kongzhong.finance.ghbank.api.params;

import com.kongzhong.finance.ghbank.api.enums.RepayInvestProductRepayType;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 18/01/2017.
 */
@Data
public class RepayInvestProductParam
{
    private RepayInvestProductRepayType repayType;
    private String oldRequestNo;
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private BigDecimal feeAmount;
    private BigDecimal amount;
    private String remark;
    private String returnUrl;
}
