package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckDepositAccountResultStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
@Builder
public class CheckDepositAccountResultDto
{
    private ResultHeader header;
    private String errorMessage;
    private String oldRequestNo;
    private CheckDepositAccountResultStatus status;
    private String transactionSequenceNo;
    private LocalDateTime transactionDatetime;
}
