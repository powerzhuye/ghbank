package com.kongzhong.finance.ghbank.api.params;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
public class SubmitInvestProductDiscountParam
{
    private String loanNo;
    private String bwAccountName;
    private String bwAccountNo;
    private Integer totalCount;
    private BigDecimal amount;
    private List<SubmitInvestProductDiscountItemParam> items;
}
