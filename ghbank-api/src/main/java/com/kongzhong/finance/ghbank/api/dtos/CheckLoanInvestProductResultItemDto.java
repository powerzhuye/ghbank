package com.kongzhong.finance.ghbank.api.dtos;

import com.kongzhong.finance.ghbank.api.enums.CheckLoanInvestProductResultRsItemStatus;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by zhuye on 11/02/2017.
 */
@Data
public class CheckLoanInvestProductResultItemDto
{
    private String requestNo;
    private String loanNo;
    private String accountNo;
    private String accountName;
    private BigDecimal amount;
    private CheckLoanInvestProductResultRsItemStatus status;
    private String errorMessage;
}
