package com.kongzhong.finance.ghbank.core.ayncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ApplicationType;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantRequest;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncCreateAccountMerchantRequest extends AsyncMerchantRequest
{
    @NotNull
    @Size(max = 128)
    @ContentField(name = "MERCHANTNAME", description = "商户名称")
    private final String merchantName;

    @NotNull
    @Size(max = 2)
    @ContentField(name = "TTRANS", description = "交易类型")
    private String transactionType = AsyncMerchantRequestOperationType.账户开立.getCode();

    @Size(max = 128)
    @ContentField(name = "ACNAME", description = "姓名")
    private String accountName;

    @Size(max = 4)
    @ContentField(name = "IDTYPE", description = "证件类型")
    private String idType = "1010";

    @Size(max = 32)
    @ContentField(name = "IDNO", description = "证件号码")
    private String idNo;

    @Size(max = 18)
    @ContentField(name = "MOBILE", description = "手机号码")
    private String mobile;

    @Size(max = 50)
    @ContentField(name = "EMAIL", description = "用户邮箱")
    private String email;

    @Size(max = 50)
    @ContentField(name = "CUSTMNGRNO", description = "客户经理编号")
    private String customerManagerNo;

    public AsyncCreateAccountMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);

        merchantName = messageContext.getMerchantName();
    }

    @Override
    public String getTransactionCode()
    {
        if (messageContext.getApplicationType().equals(ApplicationType.PC.toString()))
            return "OGW00042";
        else
            return "OGW00090";
    }
}
