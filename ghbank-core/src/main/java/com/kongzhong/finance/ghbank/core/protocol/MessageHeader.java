package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.IFromMessage;
import com.kongzhong.finance.ghbank.core.IToMessage;
import com.kongzhong.finance.ghbank.core.MessageContext;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.Optional;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class MessageHeader implements IToMessage, IFromMessage<MessageHeader>
{
    public static final int LENGTH = 16;
    private static final String VERSION = "001";
    private static final String TYPE = "X";
    private static final String RESERVED_FIELD = "          ";
    private String version;
    private String type;
    private String reversedField;
    private Priority priority;
    private InvokeMethod invokeMethod;

    @Override
    public String toMessage()
    {
        return new StringBuilder()
                .append(VERSION)
                .append(TYPE)
                .append(Optional.ofNullable(priority).orElse(Priority.NORMAL).getCode())
                .append(invokeMethod.getCode())
                .append(RESERVED_FIELD)
                .toString();
    }

    @Override
    public MessageHeader fromMessage(MessageContext messageContext, String message)
    {
        try
        {
            version = StringUtils.left(message, 3);
            type = StringUtils.mid(message, 3, 1);
            priority = Priority.parse(Integer.valueOf(StringUtils.mid(message, 4, 1)));
            invokeMethod = InvokeMethod.parse(Integer.valueOf(StringUtils.mid(message, 5, 1)));
            reversedField = StringUtils.substring(message, 6, LENGTH);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(String.format("无效的消息头:%s", message), ex);
        }
        return this;
    }
}
