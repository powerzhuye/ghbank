package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultMerchantRequest;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncCheckLoanInvestProductResultMerchantRequest extends SyncCheckResultMerchantRequest
{
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @Size(max = 28)
    @ContentField(name = "OLDTTJNL", description = "原投标流水号")
    private String oldPurchaseProductNo;

    public SyncCheckLoanInvestProductResultMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00066";
    }
}
