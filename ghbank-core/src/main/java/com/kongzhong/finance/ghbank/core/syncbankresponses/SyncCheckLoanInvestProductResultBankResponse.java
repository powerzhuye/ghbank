package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultBankResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class SyncCheckLoanInvestProductResultBankResponse extends SyncCheckResultBankResponse
{
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @ContentField(name = "BWACNAME", description = "借款人姓名")
    private String bwAccountName;

    @ContentField(name = "BWACNO", description = "借款人账号")
    private String bwAccountNo;

    @ContentField(name = "ACMNGAMT", description = "账户管理费")
    private String accountManageAmount;

    @ContentField(name = "GUARANTAMT", description = "风险保证金")
    private String guaranteeAmount;

    @ContentField(nested = true, name = "RSLIST", description = "明细投资信息")
    private List<SyncCheckLoanInvestProductResultBankResponseRsItem> rsItems = new ArrayList<>();
}
