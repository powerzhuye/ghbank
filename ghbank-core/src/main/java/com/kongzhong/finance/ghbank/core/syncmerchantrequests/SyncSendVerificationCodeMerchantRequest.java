package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncSendVerificationCodeMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 3)
    @ContentField(name = "TRSTYPE", description = "操作类型")
    private String operationType;

    @Size(max = 18)
    @ContentField(name = "MOBILE_NO", description = "手机号码")
    private String mobileNo;

    @Size(max = 32)
    @ContentField(name = "ACNO", description = "银行账号")
    private String accountNo;

    public SyncSendVerificationCodeMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00041";
    }
}
