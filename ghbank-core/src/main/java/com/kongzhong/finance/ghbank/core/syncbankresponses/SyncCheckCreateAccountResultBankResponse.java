package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultBankResponse;
import lombok.Data;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class SyncCheckCreateAccountResultBankResponse extends SyncCheckResultBankResponse
{
    @ContentField(name = "RESJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;

    @ContentField(name = "TRANSDT", description = "交易日期")
    private String transactionDate;

    @ContentField(name = "TRANSTM", description = "交易时间")
    private String transactionTime;

    @ContentField(name = "ACNO", description = "银行账号")
    private String accountNo;

    @ContentField(name = "ACNAME", description = "客户姓名")
    private String accountName;

    @ContentField(name = "IDTYPE", description = "证件类型")
    private String idType;

    @ContentField(name = "IDNO", description = "证件号码")
    private String idNo;

    @ContentField(name = "MOBILE", description = "手机号码")
    private String mobile;
}
