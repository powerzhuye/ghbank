package com.kongzhong.finance.ghbank.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.validation.ConstraintViolation;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.InflaterInputStream;

/**
 * Created by zhuye on 16/01/2017.
 */
@Log
public class Utils
{
    private static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String sign(String data, String privateKeyStr)
    {
        try
        {
            String md5 = md5(data);
            byte[] keyBytes = Base64.decodeBase64(privateKeyStr);
            PrivateKey privateKey = restorePrivateKey(keyBytes);
            byte[] encodedText = rsaEncode(md5.getBytes("UTF-8"), privateKey);
            String privateResult = byteArrayToHexString(encodedText);
            return privateResult;
        }
        catch (Exception ex)
        {
            log.info(String.format("sign error! exception:%s, data:%s, privateKeyStr:%s", ex.getMessage(), data, privateKeyStr));
            return null;
        }
    }

    public static boolean verifySign(String data, String publicKeyStr, String sign)
    {
        try
        {
            String actual = md5(data);
            PublicKey publicKey = restorePublicKey(Base64.decodeBase64(publicKeyStr));
            String expected = rsaDecode(hexStringToByteArray(sign), publicKey);
            return expected.equals(actual);
        }
        catch (Exception ex)
        {
            log.info(String.format("verifySign error! exception:%s, data:%s, publicKeyStr:%s", ex.getMessage(), data, publicKeyStr));
            return false;
        }
    }

    public static String encrypt(String data, String des3Key)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("DESede");
            DESedeKeySpec desKeySpec = new DESedeKeySpec(des3Key.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
            SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return new String(Base64.encodeBase64(cipher.doFinal(data.getBytes("UTF-8"))));
        }
        catch (Exception ex)
        {
            log.info(String.format("encrypt error! exception:%s, data:%s, des3Key:%s", ex.getMessage(), data, des3Key));
            return null;
        }
    }

    public static String decrypt(String data, String des3Key)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("DESede");
            DESedeKeySpec desKeySpec = new DESedeKeySpec(des3Key.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
            SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] dd = cipher.doFinal(Base64.decodeBase64(data.getBytes("UTF-8")));
            return new String(dd, "UTF-8");
        }
        catch (Exception ex)
        {
            log.info(String.format("decrypt error! exception:%s, data:%s, des3Key:%s", ex.getMessage(), data, des3Key));
            return null;
        }
    }

    public static List<Field> getAllFields(List<Field> fields, Class<?> type)
    {
        if (type.getSuperclass() != null)
            fields = getAllFields(fields, type.getSuperclass());
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        return fields;
    }

    private static String md5(String data)
    {
        try
        {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(StandardCharsets.UTF_8.encode(data));
            String result = String.format("%032x", new BigInteger(1, md5.digest())).toUpperCase();
            return result;
        }
        catch (Exception ex)
        {
            log.info(String.format("md5 error! exception:%s, data:%s", ex.getMessage(), data));
            return null;
        }
    }

    private static PrivateKey restorePrivateKey(byte[] keyBytes)
    {
        try
        {
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = factory.generatePrivate(pkcs8EncodedKeySpec);
            return privateKey;
        }

        catch (Exception ex)
        {
            log.info(String.format("restorePrivateKey error! exception:%s", ex.getMessage()));
            return null;
        }
    }

    private static PublicKey restorePublicKey(byte[] keyBytes)
    {
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(keyBytes);

        try
        {
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = factory.generatePublic(x509EncodedKeySpec);
            return publicKey;
        }
        catch (Exception ex)
        {
            log.info(String.format("restorePublicKey error! exception:%s", ex.getMessage()));
            return null;
        }
    }

    private static byte[] rsaEncode(byte[] plainText, PrivateKey key)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher.doFinal(plainText);
        }
        catch (Exception ex)
        {
            log.info(String.format("rsaEncode error! exception:%s", ex.getMessage()));
            return null;
        }
    }

    private static String rsaDecode(byte[] encodedText, PublicKey key)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            return new String(cipher.doFinal(encodedText));
        }
        catch (Exception ex)
        {
            log.info(String.format("rsaDecode error! exception:%s", ex.getMessage()));
            return null;
        }
    }

    private static String byteArrayToHexString(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++)
        {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);

    }

    public static byte[] hexStringToByteArray(String s)
    {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2)
        {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static <T> void checkViolations(Set<ConstraintViolation<T>> result)
    {
        if (result.size() > 0)
        {
            throw new RuntimeException(result.stream()
                    .map(item ->
                    {
                        try
                        {
                            return String.format("%s验证失败,%s %s,原始信息:%s",
                                    item.getRootBeanClass().toString(),
                                    item.getPropertyPath(),
                                    item.getMessage(),
                                    objectMapper.writeValueAsString(item.getRootBean()));
                        }
                        catch (JsonProcessingException e)
                        {
                            e.printStackTrace();
                            return String.format("%s验证失败,%s %s",
                                    item.getRootBeanClass().toString(),
                                    item.getPropertyPath(),
                                    item.getMessage());
                        }
                    })
                    .collect(Collectors.joining("\r\n")));
        }
    }

    public static String decompress(byte[] bytes, String encoding)
    {
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try
        {
            byte[] buffer = new byte[8192];
            int len;
            while ((len = in.read(buffer)) > 0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), encoding);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
