package com.kongzhong.finance.ghbank.core.protocol;

import java.lang.annotation.*;

/**
 * Created by zhuye on 16/01/2017.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
@Inherited
public @interface ContentField
{
    boolean secure() default true;

    boolean nested() default false;

    String name() default "";

    String description() default "";
}
