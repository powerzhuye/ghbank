package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.Utils;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.time.LocalDateTime;

import static org.joox.JOOX.$;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class ResponseHeader extends AbstractHeader
{
    @NotNull
    @Size(max = 20)
    private String serverFlow;

    @NotNull
    @Size(min = 8, max = 8)
    private String serverDate;

    @NotNull
    @Size(min = 6, max = 6)
    private String serverTime;

    @NotNull
    private String status;

    @NotNull
    private String errorCode;

    @NotNull
    private String errorMsg;

    @Override
    public String toMessage()
    {
        serverDate = dateFormatter.format(LocalDateTime.now());
        serverTime = timeFormatter.format(LocalDateTime.now());
        serverFlow = StringUtils.leftPad(transactionId, 20, "0");

        Utils.checkViolations(validator.validate(this));

        return new StringBuilder()
                .append("<header>")
                .append("<channelCode>".concat(channelCode).concat("</channelCode>"))
                .append("<transCode>".concat(transactionCode).concat("</transCode>"))
                .append("<channelFlow>".concat(channelFlow).concat("</channelFlow>"))
                .append("<serverFlow>".concat(serverFlow).concat("</serverFlow>"))
                .append("<serverDate>".concat(serverDate).concat("</serverDate>"))
                .append("<serverTime>".concat(serverTime).concat("</serverTime>"))
                .append("<encryptData>".concat(encryptData).concat("</encryptData>"))
                .append("<status>".concat(status).concat("</status>"))
                .append("<errorCode>".concat(errorCode).concat("</errorCode>"))
                .append("<errorMsg>".concat(errorMsg).concat("</errorMsg>"))
                .append("</header>")
                .toString();
    }

    @Override
    public ResponseHeader fromMessage(MessageContext messageContext, String message)
    {
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(message)));
            channelCode = $(document).find("channelCode").text();
            transactionCode = $(document).find("transCode").text();
            channelFlow = $(document).find("channelFlow").text();
            serverFlow = $(document).find("serverFlow").text();
            serverDate = $(document).find("serverDate").text();
            serverTime = $(document).find("serverTime").text();
            status = $(document).find("status").text();
            errorCode = $(document).find("errorCode").text();
            errorMsg = $(document).find("errorMsg").text();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return this;
    }

    @Override
    public boolean isOK()
    {
        return this.errorCode == null || this.errorCode.equals("0");
    }
}
