package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultBankResponse;
import lombok.Data;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class SyncCheckAuthAutoRepayInvestProductResultBankResponse extends SyncCheckResultBankResponse
{
    @ContentField(name = "RESJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;

    @ContentField(name = "TRANSDT", description = "交易日期")
    private String transactionDate;

    @ContentField(name = "TRANSTM", description = "交易时间")
    private String transactionTime;
}
