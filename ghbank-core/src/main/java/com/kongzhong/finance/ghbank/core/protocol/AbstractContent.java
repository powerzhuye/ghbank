package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.IFromMessage;
import com.kongzhong.finance.ghbank.core.IToMessage;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.Utils;
import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthLongestWord;
import de.vandermeer.asciitable.v2.render.WidthLongestWordMaxCol;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import lombok.Data;
import lombok.extern.java.Log;
import org.jooq.lambda.Unchecked;
import org.joox.Match;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.joox.JOOX.$;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
@Log
public abstract class AbstractContent implements IToMessage, IFromMessage<AbstractContent>
{
    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @NotNull
    @Size(min = 8, max = 8)
    @ContentField(secure = false, name = "TRANSCODE", description = "交易码")
    protected String transactionCode;

    protected MessageContext messageContext;

    public AbstractContent(MessageContext messageContext)
    {
        this.messageContext = messageContext;
    }

    public AbstractContent()
    {

    }

    public abstract InvokeMethod getInvokeMethod();

    private String encrypt(String data)
    {
        return Utils.encrypt(data, messageContext.getDes3Key());
    }

    @Override
    public String toMessage()
    {
        Utils.checkViolations(validator.validate(this));

        V2_AsciiTable at = new V2_AsciiTable();
        at.addRule();
        at.addRow("发出请求", "ID", messageContext.getTransactionId());
        at.addRule();
        at.addRow("ID", "说明", "值");
        at.addStrongRule();

        List<Field> fields = new ArrayList<>();
        Utils.getAllFields(fields, getClass());

        Function<Predicate<Field>, String> getCommandContent = condition -> fields.stream()
                .filter(field -> field.isAnnotationPresent(ContentField.class) && condition.test(field))
                .map(Unchecked.function(field ->
                {
                    field.setAccessible(true);
                    ContentField contentField = field.getAnnotation(ContentField.class);
                    String name = contentField.name();
                    String desc = contentField.description();
                    String value = "";
                    if (!contentField.nested())
                    {
                        value = Optional.ofNullable((String) field.get(this)).orElse("");
                        at.addRow(name, desc, value);
                        at.addRule();
                        return String.format("<%s>%s</%s>", name, value, name);

                    }
                    else
                    {
                        List items = (List) field.get(this);
                        if (items != null && items.size() > 0)
                        {
                            for (int i = 0; i < items.size(); i++)
                            {
                                Object item = items.get(i);
                                at.addRow(name, desc, "#" + i);
                                at.addStrongRule();
                                List<Field> itemFields = new ArrayList<>();
                                Utils.getAllFields(itemFields, item.getClass());

                                String itemContent = itemFields.stream()
                                        .filter(itemField -> itemField.isAnnotationPresent(ContentField.class))
                                        .map(Unchecked.function(itemField ->
                                        {
                                            itemField.setAccessible(true);
                                            ContentField itemContentField = itemField.getAnnotation(ContentField.class);
                                            if (!itemContentField.nested())
                                            {
                                                String itemContentFieldName = itemContentField.name();
                                                String itemContentFieldDesc = itemContentField.description();
                                                String itemValue = Optional.ofNullable((String) itemField.get(item)).orElse("");
                                                at.addRow(itemContentFieldName, itemContentFieldDesc, itemValue);
                                                at.addRule();

                                                return String.format("<%s>%s</%s>", itemContentFieldName, itemValue, itemContentFieldName);
                                            }
                                            else
                                            {
                                                //不支持更多层的嵌套
                                                return "";
                                            }
                                        })).collect(Collectors.joining(""));

                                value += String.format("<%s>%s</%s>", name, itemContent, name);
                            }
                        }
                    }
                    return value;
                }))
                .collect(Collectors.joining(""));

        Predicate<Field> secure = field -> field.getAnnotation(ContentField.class).secure();

        String insecureContent = getCommandContent.apply(secure.negate());
        at.addRow("以下为加密区域内容", null, null);
        at.addRule();
        String secureContent = getCommandContent.apply(secure);

        V2_AsciiTableRenderer render = new V2_AsciiTableRenderer();
        render.setTheme(V2_E_TableThemes.ASC7_LATEX_STYLE_STRONG.get());
        render.setWidth(new WidthLongestWordMaxCol(80));

        log.info(render.render(at).toString());
        log.info("请求未加密内容：" + new StringBuilder()
                .append("<body>")
                .append(insecureContent)
                .append("<XMLPARA>")
                .append(secureContent)
                .append("</XMLPARA>")
                .append("</body>")
                .toString());

        return new StringBuilder()
                .append("<body>")
                .append(insecureContent)
                .append("<XMLPARA>")
                .append(encrypt(secureContent))
                .append("</XMLPARA>")
                .append("</body>")
                .toString();
    }

    @Override
    public AbstractContent fromMessage(MessageContext messageContext, String message)
    {
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(message)));

            String secureData = $(document).find("XMLPARA").text();
            String decryptedXml = decrypt(secureData, messageContext.getDes3Key());
            Document secureDoc = builder.parse(new InputSource(new StringReader("<root>".concat(decryptedXml).concat("</root>"))));

            log.info("响应解密后内容：" + message.replace(secureData, decryptedXml));

            List<Field> fields = new ArrayList<>();
            Utils.getAllFields(fields, getClass());

            V2_AsciiTable at = new V2_AsciiTable();
            at.addRule();
            at.addRow("收到响应", null, null);
            at.addRule();
            at.addRow("ID", "说明", "值");
            at.addStrongRule();

            fields.stream()
                    .filter(field -> field.isAnnotationPresent(ContentField.class))
                    .forEach(field ->
                    {
                        field.setAccessible(true);
                        ContentField contentField = field.getAnnotation(ContentField.class);
                        String name = contentField.name();
                        String desc = contentField.description();
                        try
                        {
                            if (!contentField.nested())
                            {
                                Match match = null;
                                if (contentField.secure())
                                {
                                    match = $(secureDoc).find(name);
                                }
                                else
                                {
                                    match = $(document).find(name);
                                }
                                if (match != null)
                                {
                                    if (field.getType().isEnum())
                                    {
                                        field.set(this, Enum.valueOf((Class<Enum>) field.getType(), match.text()));
                                    }
                                    else
                                    {
                                        field.set(this, match.text());
                                    }
                                }
                                at.addRow(name, desc, match.text());
                                at.addRule();
                            }
                            else
                            {
                                List items = (List) field.get(this);
                                Type genericType = field.getGenericType();
                                Class itemClass = (Class) ((ParameterizedType) genericType).getActualTypeArguments()[0];
                                if (items != null)
                                {
                                    List<Match> itemsContent = $(secureDoc).find(name).each();
                                    for (int i = 0; i < itemsContent.size(); i++)
                                    {
                                        String itemContent = itemsContent.get(i).content();
                                        at.addRow(name, desc, "#" + i);
                                        at.addStrongRule();
                                        Object item = itemClass.newInstance();
                                        List<Field> itemFields = new ArrayList<>();
                                        Utils.getAllFields(itemFields, itemClass);

                                        itemFields.stream()
                                                .filter(itemField -> itemField.isAnnotationPresent(ContentField.class))
                                                .forEach(itemField ->
                                                {
                                                    try
                                                    {
                                                        itemField.setAccessible(true);
                                                        ContentField itemContentField = itemField.getAnnotation(ContentField.class);
                                                        String itemName = itemContentField.name();
                                                        String itemDesc = itemContentField.description();

                                                        if (!itemContentField.nested())
                                                        {
                                                            Document itemDoc = builder.parse(new InputSource(new StringReader("<root>".concat(itemContent).concat("</root>"))));
                                                            Match match = $(itemDoc).find(itemName);
                                                            if (match != null)
                                                            {
                                                                String itemValue = match.text();
                                                                if (field.getClass().isEnum())
                                                                {
                                                                    itemField.set(item, Enum.valueOf((Class<Enum>) itemField.getType(), itemValue));
                                                                }
                                                                else
                                                                {
                                                                    itemField.set(item, itemValue);
                                                                }
                                                                at.addRow(itemName, itemDesc, itemValue);
                                                                at.addRule();
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        ex.printStackTrace();
                                                    }
                                                });


                                        Method add = List.class.getDeclaredMethod("add", Object.class);
                                        add.invoke(items, item);
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    });

            V2_AsciiTableRenderer render = new V2_AsciiTableRenderer();
            render.setTheme(V2_E_TableThemes.ASC7_LATEX_STYLE_STRONG.get());
            render.setWidth(new WidthLongestWord());
            log.info(render.render(at).toString());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return this;
    }

    private String decrypt(String data, String key)
    {
        return Utils.decrypt(data, key);
    }

}
