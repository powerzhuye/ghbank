package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultBankResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class SyncCheckSubmitInvestProductDiscountResultBankResponse extends SyncCheckResultBankResponse
{
    @ContentField(name = "RESJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;

    @ContentField(name = "TRANSDT", description = "交易日期")
    private String transactionDate;

    @ContentField(name = "TRANSTM", description = "交易时间")
    private String transactionTime;

    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @ContentField(name = "BWACNAME", description = "还款账号户名")
    private String bwAccountName;

    @ContentField(name = "BWACNO", description = "还款账号")
    private String bwAccountNo;

    @ContentField(name = "AMOUNT", description = "优惠总金额")
    private String amount;

    @ContentField(name = "TOTALNUM", description = "优惠总笔数")
    private String totalCount;

    @ContentField(nested = true, name = "RSLIST", description = "优惠明细")
    private List<SyncCheckSubmitInvestProductDiscountResultBankResponseItem> items = new ArrayList<>();

}
