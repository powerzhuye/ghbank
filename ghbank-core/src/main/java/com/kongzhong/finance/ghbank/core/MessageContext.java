package com.kongzhong.finance.ghbank.core;

import com.kongzhong.finance.ghbank.core.protocol.Priority;
import lombok.Builder;
import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
@Builder
public class MessageContext
{
    private String transactionId;
    private String bankPublicKey;
    private String merchantPrivateKey;
    private String des3Key;
    private String channelCode;
    private String merchantId;
    private String merchantName;
    private String bankUrl;

    private Priority priority;
    private String applicationType;
}
