package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncAutoPurchaseInvestProductMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "AMOUNT", description = "投标金额")
    private String amount;

    @Size(max = 60)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    public SyncAutoPurchaseInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00059";
    }
}
