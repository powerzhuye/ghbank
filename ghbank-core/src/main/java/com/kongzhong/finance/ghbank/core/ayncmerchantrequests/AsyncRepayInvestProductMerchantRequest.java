package com.kongzhong.finance.ghbank.core.ayncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ApplicationType;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantRequest;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncRepayInvestProductMerchantRequest extends AsyncMerchantRequest
{
    @NotNull
    @Size(max = 128)
    @ContentField(name = "MERCHANTNAME", description = "商户名称")
    private final String merchantName;

    @NotNull
    @Size(max = 20)
    @ContentField(name = "DFFLAG", description = "还款类型")
    private String repayType;

    @Size(max = 28)
    @ContentField(name = "OLDREQSEQNO", description = "原垫付请求流水号")
    private String oldRequestNo;

    @NotNull
    @Size(max = 20)
    @ContentField(name = "TTRANS", description = "交易类型")
    private String transactionType = AsyncMerchantRequestOperationType.互联网借贷还款.getCode();

    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "BWACNAME", description = "还款账号户名")
    private String bwAccountName;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "BWACNO", description = "还款账号")
    private String bwAccountNo;

    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "AMOUNT", description = "还款金额")
    private String amount;

    @Size(max = 60)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    //@Digits(integer = 13, fraction = 2)
    @ContentField(name = "FEEAMT", description = "手续费")
    private String feeAmount;

    public AsyncRepayInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);

        merchantName = messageContext.getMerchantName();

    }

    @Override
    public String getTransactionCode()
    {
        if (messageContext.getApplicationType().equals(ApplicationType.PC.toString()))
            return "OGW00067";
        else
            return "OGW00095";
    }
}
