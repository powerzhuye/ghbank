package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class ResponseException extends Exception
{
    private final ResponseStatus status;
    private final String errorCode;
    private final String errorMessage;

    public ResponseException(ResponseStatus status, String errorCode, String errorMessage)
    {
        super(errorMessage);

        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.status = status;
    }
}
