package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultMerchantRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncCheckWithdrawAccountResultMerchantRequest extends SyncCheckResultMerchantRequest
{
    @NotNull
    @Size(min = 8, max = 8)
    @ContentField(name = "TRANSDT", description = "原提现交易日期")
    private String transactionDate;

    public SyncCheckWithdrawAccountResultMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00048";
    }
}
