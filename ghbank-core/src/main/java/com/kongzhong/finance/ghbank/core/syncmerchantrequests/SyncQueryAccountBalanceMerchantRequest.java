package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncQueryAccountBalanceMerchantRequest extends SyncMerchantRequest
{
    @Size(max = 5)
    @ContentField(name = "BUSTYPE", description = "业务类型")
    private final String businessType = "";

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNO", description = "银行账号")
    private String accountNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "ACNAME", description = "账号户名")
    private String accountName;

    public SyncQueryAccountBalanceMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00049";
    }
}
