package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum ResponseStatus
{
    SUCCESS("0"),
    FAILURE("1"),
    IN_PROGRESS("2"),
    UNKNOWN("3"),
    TEST_MODE("testMode");

    @Getter
    private final String code;

    public static ResponseStatus parse(String code)
    {
        return Arrays.stream(values()).filter(item -> item.code.equals(code))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%s", ResponseStatus.class.getName(), code)));
    }
}
