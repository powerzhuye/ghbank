package com.kongzhong.finance.ghbank.core.ayncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ApplicationType;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantRequest;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncWithdrawAccountMerchantRequest extends AsyncMerchantRequest
{
    @NotNull
    @Size(max = 128)
    @ContentField(name = "MERCHANTNAME", description = "商户名称")
    private final String merchantName;

    @NotNull
    @Size(max = 2)
    @ContentField(name = "TTRANS", description = "交易类型")
    private String operationType = AsyncMerchantRequestOperationType.提现.getCode();

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNO", description = "银行账号")
    private String accountNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "ACNAME", description = "账号户名")
    private String accountName;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "AMOUNT", description = "交易金额")
    private String amount;

    @Size(max = 20)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    public AsyncWithdrawAccountMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);

        merchantName = messageContext.getMerchantName();
    }

    @Override
    public String getTransactionCode()
    {
        if (messageContext.getApplicationType().equals(ApplicationType.PC.toString()))
            return "OGW00047";
        else
            return "OGW00093";
    }
}
