package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncBankResponse;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncLoanInvestProductBankResponse extends SyncBankResponse
{
    @ContentField(name = "RESJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;

    @ContentField(name = "TRANSDT", description = "交易日期")
    private String transactionDate;

    @ContentField(name = "TRANSTM", description = "交易时间")
    private String transactionTime;
}
