package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncSubmitRepayInvestProductIncomeMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 28)
    @ContentField(name = "OLDREQSEQNO", description = "原交易流水号")
    private String oldRequestNo;

    @NotNull
    @Size(max = 1)
    @ContentField(name = "DFFLAG", description = "还款类型")
    private String repayType;

    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "BWACNAME", description = "还款账号户名")
    private String bwAccountName;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "BWACNO", description = "还款账号")
    private String bwAccountNo;

    @NotNull
    @Digits(integer = 10, fraction = 0)
    @ContentField(name = "TOTALNUM", description = "总笔数")
    private String totalCount;

    @NotNull
    @ContentField(nested = true, name = "REPAYLIST", description = "明细收益投资人")
    private List<SyncSubmitRepayInvestProductIncomeMerchantRequestRepayItem> repayItemList;

    public SyncSubmitRepayInvestProductIncomeMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00074";
    }
}
