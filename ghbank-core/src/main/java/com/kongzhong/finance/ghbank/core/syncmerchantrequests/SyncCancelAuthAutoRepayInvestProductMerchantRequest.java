package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncCancelAuthAutoRepayInvestProductMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "BWACNAME", description = "还款账号户名")
    private String bwAccountName;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "BWACNO", description = "还款账号")
    private String bwAccountNo;

    @Size(max = 60)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    @NotNull
    @Size(max = 60)
    @ContentField(name = "OTPSEQNO", description = "动态密码唯一标识")
    private String messageSequenceNo;

    @NotNull
    @Size(max = 10)
    @ContentField(name = "OTPNO", description = "动态密码")
    private String messageNo;

    public SyncCancelAuthAutoRepayInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00071";
    }
}
