package com.kongzhong.finance.ghbank.core.ayncmerchantrequests;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum AsyncMerchantRequestOperationType
{
    绑卡("1"),
    债券转让("2"),
    互联网借贷投标("4"),
    互联网借贷还款("5"),
    账户开立("6"),
    充值("7"),
    提现("8"),
    自动投标授权("9"),
    自动还款授权("10");

    @Getter
    private final String code;
}
