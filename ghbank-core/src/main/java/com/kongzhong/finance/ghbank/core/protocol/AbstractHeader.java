package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.IFromMessage;
import com.kongzhong.finance.ghbank.core.IToMessage;
import lombok.Data;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.format.DateTimeFormatter;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public abstract class AbstractHeader implements IToMessage, IFromMessage
{
    protected static final int TRANSACTION_ID_LENGTH = 11;
    protected static final int TRANSACTION_CODE_LENGTH = 3;
    protected static final String encryptData = "";
    protected static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    protected static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    protected static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");

    protected String transactionId;

    @NotNull
    @Size(min = 6, max = 6)
    protected String channelCode;

    @NotNull
    @Size(min = 8, max = 8)
    protected String transactionCode;

    @NotNull
    protected String channelFlow;

    public abstract boolean isOK();

}
