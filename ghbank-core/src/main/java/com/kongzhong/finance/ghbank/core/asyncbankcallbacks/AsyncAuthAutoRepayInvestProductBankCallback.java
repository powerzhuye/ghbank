package com.kongzhong.finance.ghbank.core.asyncbankcallbacks;

import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallback;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class AsyncAuthAutoRepayInvestProductBankCallback extends AsyncBankCallback
{
    @ContentField(name = "RESJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;
}
