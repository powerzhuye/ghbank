package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncCancelPurchaseInvestProductMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 28)
    @ContentField(name = "OLDREQSEQNO", description = "原投标流水号")
    private String oldRequestNo;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @Size(max = 128)
    @ContentField(name = "CANCELREASON", description = "撤标原因")
    private String cancelReason;

    public SyncCancelPurchaseInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00060";
    }
}
