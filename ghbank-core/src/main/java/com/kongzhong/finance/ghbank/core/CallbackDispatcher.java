package com.kongzhong.finance.ghbank.core;

import com.kongzhong.finance.ghbank.core.asyncbankcallbacks.*;
import com.kongzhong.finance.ghbank.core.asyncmerchantresponses.*;
import com.kongzhong.finance.ghbank.core.protocol.*;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zhuye on 22/01/2017.
 */
@Service
@Log
public class CallbackDispatcher
{
    @Autowired
    private ICallbackHandler<AsyncCreateAccountBankCallback, AsyncBankCallbackMessageBody<AsyncCreateAccountBankCallback>, AsyncCreateAccountMerchantResponse> createAccountCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncBindCardBankCallback, AsyncBankCallbackMessageBody<AsyncBindCardBankCallback>, AsyncBindCardMerchantResponse> bindCardCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncDepositAccountBankCallback, AsyncBankCallbackMessageBody<AsyncDepositAccountBankCallback>, AsyncDepositAccountMerchantResponse> depositAccountCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncWithdrawAccountBankCallback, AsyncBankCallbackMessageBody<AsyncWithdrawAccountBankCallback>, AsyncWithdrawAccountMerchantResponse> withdrawAccountCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncPurchaseInvestProductBankCallback, AsyncBankCallbackMessageBody<AsyncPurchaseInvestProductBankCallback>, AsyncPurchaseInvestProductMerchantResponse> purchaseInvestProductCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncTransferInvestProductBankCallback, AsyncBankCallbackMessageBody<AsyncTransferInvestProductBankCallback>, AsyncTransferInvestProductMerchantResponse> transferInvestProductCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncAuthAutoPurchaseInvestProductBankCallback, AsyncBankCallbackMessageBody<AsyncAuthAutoPurchaseInvestProductBankCallback>, AsyncAuthAutoPurchaseInvestProductMerchantResponse> authAutoPurchaseInvestProductCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncRepayInvestProductBankCallback, AsyncBankCallbackMessageBody<AsyncRepayInvestProductBankCallback>, AsyncRepayInvestProductMerchantResponse> repayInvestProductCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncAuthAutoRepayInvestProductBankCallback, AsyncBankCallbackMessageBody<AsyncAuthAutoRepayInvestProductBankCallback>, AsyncAuthAutoRepayInvestProductMerchantResponse> authAutoRepayInvestProductCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncCancelPurchaseInvestProductBankCallback, AsyncBankCallbackMessageBody<AsyncCancelPurchaseInvestProductBankCallback>, AsyncCancelPurchaseInvestProductMerchantResponse> cancelInvestProductCallbackHandler;

    @Autowired
    private ICallbackHandler<AsyncDiscardInvestProductBankCallback, AsyncBankCallbackMessageBody<AsyncDiscardInvestProductBankCallback>, AsyncDiscardInvestProductMerchantResponse> discardInvestProductCallbackHandler;


    private <ABC extends AsyncBankCallback,
            ABCM extends AsyncBankCallbackMessageBody<ABC>,
            AMR extends AsyncMerchantResponse,
            AMRM extends AsyncMerchantResponseMessageBody<AMR>> String handle(String data,
                                                                              MessageContext context,
                                                                              Message<ResponseHeader, ABC, ABCM> callbackMessage,
                                                                              Message<ResponseHeader, AMR, AMRM> responseMessage,
                                                                              ICallbackHandler<ABC, ABCM, AMR> handler)
    {
        ABCM callbackMessageBody = callbackMessage.getMessageBody();
        AMRM responseMessageBody = responseMessage.getMessageBody();
        callbackMessage.fromMessage(context, data);
        if (handler == null)
            throw new RuntimeException("找不到处理器来处理回调消息:" + callbackMessageBody);

        log.info("收到回调内容<<" + callbackMessageBody.getContent());
        try
        {
            responseMessageBody.setContent(handler.handle(callbackMessage.getMessageBody(), context));
            responseMessageBody.getContent().setReturnCode("000000");
            responseMessageBody.getContent().setReturnMessage("交易成功");

            ResponseHeader header = responseMessageBody.getHeader();
            if (header != null)
            {
                header.setErrorCode("0");
                header.setErrorMsg("");
                header.setStatus(ResponseStatus.SUCCESS.getCode());
            }
        }
        catch (ResponseException ex)
        {
            responseMessageBody.getContent().setReturnCode(ex.getErrorCode());
            responseMessageBody.getContent().setReturnMessage(ex.getErrorMessage());
            ResponseHeader header = responseMessageBody.getHeader();
            if (header != null)
            {
                header.setErrorCode(ex.getErrorCode());
                header.setErrorMsg(ex.getErrorMessage());
                header.setStatus(ResponseStatus.FAILURE.getCode());
            }
        }

        String transactionCode = callbackMessage.getMessageBody().getContent().getTransactionCode();
        responseMessageBody.getHeader().setTransactionCode(transactionCode);
        responseMessageBody.getHeader().setChannelFlow(callbackMessageBody.getHeader().getChannelFlow());
        responseMessageBody.getContent().setTransactionCode(transactionCode);
        responseMessageBody.getContent().setOldRequestNo(callbackMessageBody.getContent().getOldRequestNo());

        log.info("发出响应内容>>" + responseMessageBody.getContent());
        return responseMessage.toMessage();
    }

    public String callback(String data, MessageContext context)
    {
        String transactionCode = Message.getTransactionCode(data);
        switch (transactionCode)
        {
            case "OGWR0001":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncCreateAccountBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncCreateAccountMerchantResponse(context))),
                        createAccountCallbackHandler);
            }
            case "OGWR0002":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncBindCardBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncBindCardMerchantResponse(context))),
                        bindCardCallbackHandler);
            }
            case "OGWR0003":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncDepositAccountBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncDepositAccountMerchantResponse(context))),
                        depositAccountCallbackHandler);
            }
            case "OGWR0004":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncWithdrawAccountBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncWithdrawAccountMerchantResponse(context))),
                        withdrawAccountCallbackHandler);
            }
            case "OGWR0005":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncPurchaseInvestProductBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncPurchaseInvestProductMerchantResponse(context))),
                        purchaseInvestProductCallbackHandler);
            }
            case "OGWR0006":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncAuthAutoPurchaseInvestProductBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncAuthAutoPurchaseInvestProductMerchantResponse(context))),
                        authAutoPurchaseInvestProductCallbackHandler);
            }
            case "OGWR0007":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncTransferInvestProductBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncTransferInvestProductMerchantResponse(context))),
                        transferInvestProductCallbackHandler);
            }
            case "OGWR0008":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncRepayInvestProductBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncRepayInvestProductMerchantResponse(context))),
                        repayInvestProductCallbackHandler);
            }
            case "OGWR0011":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncAuthAutoRepayInvestProductBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncAuthAutoRepayInvestProductMerchantResponse(context))),
                        authAutoRepayInvestProductCallbackHandler);
            }
            case "OGW0014T":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncCancelPurchaseInvestProductBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncCancelPurchaseInvestProductMerchantResponse(context))),
                        cancelInvestProductCallbackHandler);
            }
            case "OGW0015T":
            {
                return handle(data, context,
                        new Message<>(context, new AsyncBankCallbackMessageBody<>(new AsyncDiscardInvestProductBankCallback())),
                        new Message<>(context, new AsyncMerchantResponseMessageBody<>(new AsyncDiscardInvestProductMerchantResponse(context))),
                        discardInvestProductCallbackHandler);
            }
        }

        throw new RuntimeException(String.format("未识别的交易码(%s):%s", transactionCode, data));
    }
}
