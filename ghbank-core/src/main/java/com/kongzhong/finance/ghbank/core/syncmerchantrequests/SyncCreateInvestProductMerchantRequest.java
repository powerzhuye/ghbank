package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncCreateInvestProductMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 128)
    @ContentField(name = "MERCHANTNAME", description = "商户名称")
    private final String merchantName;

    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "INVESTID", description = "标的编号")
    private String investId;

    @NotNull
    @Size(max = 512)
    @ContentField(name = "INVESTOBJNAME", description = "标的名称")
    private String productName;

    @Size(max = 1028)
    @ContentField(name = "INVESTOBJINFO", description = "标的简介")
    private String productDesc;

    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "MININVESTAMT", description = "最低投标金额")
    private String minInvestAmount;

    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "MAXINVESTAMT", description = "最高投标金额")
    private String maxInvestAmount;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "INVESTOBJAMT", description = "总标的金额")
    private String investTotalAmount;

    @NotNull
    @Size(min = 8, max = 8)
    @ContentField(name = "INVESTBEGINDATE", description = "招标开始日期")
    private String investBeginDate;

    @NotNull
    @Size(min = 8, max = 8)
    @ContentField(name = "INVESTENDDATE", description = "招标到期日期")
    private String investEndDate;

    @Size(min = 8, max = 8)
    @ContentField(name = "REPAYDATE", description = "还款日期")
    private String repayDate;

    @NotNull
    @Digits(integer = 3, fraction = 6)
    @ContentField(name = "YEARRATE", description = "年利率")
    private String yearRate;

    @NotNull
    @Digits(integer = 10, fraction = 0)
    @ContentField(name = "INVESTRANGE", description = "期限")
    private String investRange;

    @Size(max = 128)
    @ContentField(name = "RATESTYPE", description = "计息方式")
    private String rateType;

    @Size(max = 128)
    @ContentField(name = "REPAYSTYPE", description = "还款方式")
    private String repayType;

    @NotNull
    @Size(max = 3)
    @ContentField(name = "INVESTOBJSTATE", description = "标的状态")
    private String productState;

    @NotNull
    @Digits(integer = 10, fraction = 0)
    @ContentField(name = "BWTOTALNUM", description = "借款人总数")
    private String borrowerTotalCount;

    @Size(max = 512)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    @Size(max = 1)
    @ContentField(name = "ZRFLAG", description = "是否为债券转让标的")
    private String isTransfer = "0";

    @Size(max = 64)
    @ContentField(name = "REFLOANNO", description = "债券转让原标的")
    private String refLoanNo;

    @Size(max = 28)
    @ContentField(name = "OLDREQSEQ", description = "原投标第三方交易流水号")
    private String oldRequestSequence;

    @ContentField(nested = true, name = "BWLIST", description = "借款人列表")
    private List<SyncCreateInvestProductMerchantRequestBorrowerItem> borrowerItemList;

    public SyncCreateInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);

        this.merchantName = messageContext.getMerchantName();

    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00051";
    }
}
