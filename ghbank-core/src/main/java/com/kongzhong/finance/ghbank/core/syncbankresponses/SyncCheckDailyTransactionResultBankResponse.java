package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultBankResponse;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncCheckDailyTransactionResultBankResponse extends SyncCheckResultBankResponse
{
    @ContentField(name = "OPERFLAG", description = "对账类型")
    private String operationType;

    @ContentField(name = "CHECKDATE", description = "对账日期")
    private String checkDate;

    @ContentField(name = "FILENAME", description = "文件名称")
    private String fileName;

    @ContentField(name = "FILECONTEXT", description = "文件内容")
    private String fileContent;
}
