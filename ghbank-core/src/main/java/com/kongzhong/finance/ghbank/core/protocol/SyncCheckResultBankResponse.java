package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public abstract class SyncCheckResultBankResponse extends SyncBankResponse
{
    @ContentField(name = "RETURN_STATUS", description = "交易状态")
    private String returnStatus;

    @ContentField(name = "ERRORMSG", description = "失败原因")
    private String errorMessage;

    @ContentField(name = "OLDREQSEQNO", description = "原交易流水号")
    private String oldRequestNo;
}
