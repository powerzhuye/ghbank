package com.kongzhong.finance.ghbank.core.asyncmerchantresponses;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class AsyncAuthAutoRepayInvestProductMerchantResponse extends AsyncMerchantResponse
{
    @Size(max = 28)
    @ContentField(name = "REQSEQNO", description = "交易流水号")
    private String requestNo;

    @Size(max = 28)
    @ContentField(name = "RESJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;//??

    public AsyncAuthAutoRepayInvestProductMerchantResponse(MessageContext messageContext)
    {
        super(messageContext);
    }
}
