package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class SyncCheckSubmitInvestProductDiscountResultBankResponseItem
{
    @ContentField(name = "SUBSEQNO", description = "子流水号")
    private String subSequenceNo;

    @ContentField(name = "OLDREQSEQNO", description = "原投标流水号")
    private String oldRequestNo;

    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @ContentField(name = "AMOUNT", description = "优惠金额")
    private String amount;

    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    @ContentField(name = "STATUS", description = "状态")
    private String status;

    @ContentField(name = "ERRORMSG", description = "错误原因")
    private String errorMessage;

}
