package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.Utils;
import lombok.Data;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class RequestHeader extends AbstractHeader
{
    @NotNull
    private String transactionId;

    @NotNull
    @Size(min = 8, max = 8)
    private String channelDate;

    @NotNull
    @Size(min = 6, max = 6)
    private String channelTime;

    @Override
    public String toMessage()
    {
        channelDate = dateFormatter.format(LocalDateTime.now());
        channelTime = timeFormatter.format(LocalDateTime.now());

        channelFlow = channelCode
                .concat(channelDate)
                .concat(StringUtils.right(transactionCode, TRANSACTION_CODE_LENGTH))
                .concat(StringUtils.leftPad(transactionId, TRANSACTION_ID_LENGTH, "0"));

        Utils.checkViolations(validator.validate(this));

        return new StringBuilder()
                .append("<header>")
                .append("<channelCode>".concat(channelCode).concat("</channelCode>"))
                .append("<channelFlow>".concat(channelFlow).concat("</channelFlow>"))
                .append("<channelDate>".concat(channelDate).concat("</channelDate>"))
                .append("<channelTime>".concat(channelTime).concat("</channelTime>"))
                .append("<encryptData>".concat(encryptData).concat("</encryptData>"))
                .append("</header>")
                .toString();
    }

    @Override
    public RequestHeader fromMessage(MessageContext messageContext, String message)
    {
        throw new NotImplementedException();
    }

    @Override
    public boolean isOK()
    {
        throw new NotImplementedException();
    }
}
