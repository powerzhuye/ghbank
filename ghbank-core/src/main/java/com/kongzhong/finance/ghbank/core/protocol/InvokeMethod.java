package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum InvokeMethod
{
    SYNC(1),
    ASYNC(2);

    @Getter
    private final int code;

    public static InvokeMethod parse(int code)
    {
        return Arrays.stream(values()).filter(item -> item.code == code)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%d", InvokeMethod.class.getName(), code)));
    }
}
