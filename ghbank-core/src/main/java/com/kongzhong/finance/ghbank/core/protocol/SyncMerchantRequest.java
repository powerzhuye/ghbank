package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.MessageContext;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public abstract class SyncMerchantRequest extends AbstractContent
{
    @NotNull
    @Size(max = 32)
    @ContentField(name = "MERCHANTID", description = "商户唯一编号")
    private String merchantId;

    @NotNull
    @Size(max = 3)
    @ContentField(name = "APPID", description = "应用标识")
    private String applicationType;

    @Size(max = 300)
    @ContentField(name = "EXT_FILED1", description = "备用字段1")
    private String extraField1;

    @Size(max = 300)
    @ContentField(name = "EXT_FILED2", description = "备用字段2")
    private String extraField2;

    @Size(max = 300)
    @ContentField(name = "EXT_FILED3", description = "备用字段3")
    private String extraField3;

    public SyncMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
        this.transactionCode = getTransactionCode();
        this.merchantId = messageContext.getMerchantId();
        this.applicationType = messageContext.getApplicationType().toString();
    }

    public abstract String getTransactionCode();

    @Override
    public InvokeMethod getInvokeMethod()
    {
        return InvokeMethod.SYNC;
    }
}
