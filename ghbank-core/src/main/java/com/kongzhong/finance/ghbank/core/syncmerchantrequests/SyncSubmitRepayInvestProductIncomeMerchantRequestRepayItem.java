package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
public class SyncSubmitRepayInvestProductIncomeMerchantRequestRepayItem
{
    @NotNull
    @Size(max = 32)
    @ContentField(name = "SUBSEQNO", description = "子流水号")
    private String subSequenceNo;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @Size(min = 8, max = 8)
    @ContentField(name = "INCOMEDATE", description = "该收益所属截止日期")
    private String incomeDate;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "AMOUNT", description = "还款总金额")
    private String amount;

    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "PRINCIPALAMT", description = "本次还款本金")
    private String principalAmount;

    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "INCOMEAMT", description = "本次还款收益")
    private String incomeAmount;

    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "FEEAMT", description = "本次还款费用")
    private String feeAmount;

}