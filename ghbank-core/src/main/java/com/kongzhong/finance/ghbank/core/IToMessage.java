package com.kongzhong.finance.ghbank.core;

/**
 * Created by zhuye on 16/01/2017.
 */
public interface IToMessage
{
    String toMessage();
}
