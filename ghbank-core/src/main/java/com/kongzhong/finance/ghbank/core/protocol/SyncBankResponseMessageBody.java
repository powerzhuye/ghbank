package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncBankResponseMessageBody<Content extends SyncBankResponse>
        extends MessageBody<ResponseHeader, Content>
{
    public SyncBankResponseMessageBody(Content content)
    {
        super(new ResponseHeader(), content);
    }
}
