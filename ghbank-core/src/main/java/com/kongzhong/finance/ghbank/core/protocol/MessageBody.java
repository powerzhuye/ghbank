package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.IFromMessage;
import com.kongzhong.finance.ghbank.core.IToMessage;
import com.kongzhong.finance.ghbank.core.MessageContext;
import lombok.Data;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

import static org.joox.JOOX.$;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public abstract class MessageBody<Header extends AbstractHeader, Content extends AbstractContent>
        implements IToMessage, IFromMessage<MessageBody<Header, Content>>
{
    private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
    protected Content content;
    protected Header header;

    public MessageBody(Header header, Content content)
    {
        this.header = header;
        this.content = content;

        this.header.setTransactionCode(content.getTransactionCode());
    }

    public void init(MessageContext messageContext)
    {
        this.header.setTransactionId(messageContext.getTransactionId());
        this.header.setChannelCode(messageContext.getChannelCode());
    }

    @Override
    public String toMessage()
    {
        return new StringBuilder()
                .append(XML_HEADER)
                .append("<Document>")
                .append(header.toMessage())
                .append(content.toMessage())
                .append("</Document>")
                .toString();
    }

    @Override
    public MessageBody<Header, Content> fromMessage(MessageContext messageContext, String message)
    {
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(message)));
            header.fromMessage(messageContext, $(document).find("header").toString());
            if (header.isOK())
                content.fromMessage(messageContext, $(document).find("body").toString());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return this;
    }
}
