package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncCancelAuthAutoPurchaseInvestProductMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @Size(max = 60)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    @NotNull
    @Size(max = 60)
    @ContentField(name = "OTPSEQNO", description = "动态密码唯一标识")
    private String messageSequenceNo;

    @NotNull
    @Size(max = 10)
    @ContentField(name = "OTPNO", description = "动态密码")
    private String messageNo;

    public SyncCancelAuthAutoPurchaseInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00058";
    }
}
