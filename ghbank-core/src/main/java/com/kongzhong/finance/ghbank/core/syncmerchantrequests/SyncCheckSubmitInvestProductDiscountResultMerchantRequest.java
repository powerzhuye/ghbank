package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultMerchantRequest;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncCheckSubmitInvestProductDiscountResultMerchantRequest extends SyncCheckResultMerchantRequest
{
    @Size(max = 32)
    @ContentField(name = "SUBSEQNO", description = "子流水号")
    private String subSequenceNo;

    public SyncCheckSubmitInvestProductDiscountResultMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00055";
    }
}
