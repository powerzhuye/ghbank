package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncMerchantRequestMessageBody<Content extends AsyncMerchantRequest> extends MessageBody<RequestHeader, Content>
{
    public AsyncMerchantRequestMessageBody(Content content)
    {
        super(new RequestHeader(), content);
    }
}
