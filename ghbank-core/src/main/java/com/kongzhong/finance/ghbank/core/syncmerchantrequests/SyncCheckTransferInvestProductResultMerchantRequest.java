package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultMerchantRequest;
import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncCheckTransferInvestProductResultMerchantRequest extends SyncCheckResultMerchantRequest
{
    public SyncCheckTransferInvestProductResultMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00062";
    }
}
