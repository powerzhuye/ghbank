package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.MessageContext;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public abstract class AsyncMerchantResponse extends AbstractContent
{
    @NotNull
    @Size(max = 64)
    @ContentField(name = "RETURNCODE", description = "响应码")
    private String returnCode;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "RETURNMSG", description = "响应信息")
    private String returnMessage;

    @NotNull
    @Size(max = 28)
    @ContentField(name = "OLDREQSEQNO", description = "原交易流水号")
    private String oldRequestNo;

    public AsyncMerchantResponse(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public InvokeMethod getInvokeMethod()
    {
        return InvokeMethod.ASYNC;
    }
}
