package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.IToMessage;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class MessageSecurityField implements IToMessage
{
    public final static int SIGNATURE_LENGTH_LENGTH = 8;
    private String signature;

    @Override
    public String toMessage()
    {
        return StringUtils.leftPad(String.valueOf(signature.length()), SIGNATURE_LENGTH_LENGTH, "0")
                .concat(signature);
    }
}
