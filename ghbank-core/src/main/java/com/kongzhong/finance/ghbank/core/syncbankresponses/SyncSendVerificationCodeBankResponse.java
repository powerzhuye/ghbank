package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncBankResponse;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncSendVerificationCodeBankResponse extends SyncBankResponse
{
    @ContentField(name = "OTPSEQNO", description = "短信唯一标识")
    private String messageSequenceNo;

    @ContentField(name = "OTPINDEX", description = "短信序号")
    private String messageIndex;
}
