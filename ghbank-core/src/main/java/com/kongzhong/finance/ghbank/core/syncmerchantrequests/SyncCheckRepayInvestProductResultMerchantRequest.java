package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultMerchantRequest;
import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncCheckRepayInvestProductResultMerchantRequest extends SyncCheckResultMerchantRequest
{
    public SyncCheckRepayInvestProductResultMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00068";
    }
}
