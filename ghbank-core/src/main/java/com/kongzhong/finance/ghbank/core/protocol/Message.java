package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.IFromMessage;
import com.kongzhong.finance.ghbank.core.IToMessage;
import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.Utils;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

import static org.joox.JOOX.$;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class Message<
        Header extends AbstractHeader,
        Content extends AbstractContent,
        Body extends MessageBody<Header, Content>> implements IToMessage, IFromMessage<Message<Header, Content, Body>>
{
    private final MessageContext messageContext;
    private MessageHeader messageHeader;
    private Body messageBody;

    public Message(MessageContext messageContext, Body messageBody)
    {
        this.messageContext = messageContext;

        this.messageHeader = new MessageHeader();
        this.messageHeader.setInvokeMethod(messageBody.getContent().getInvokeMethod());
        this.messageHeader.setPriority(messageContext.getPriority());

        this.messageBody = messageBody;
        this.messageBody.init(messageContext);
    }

    public static String getTransactionCode(String message)
    {
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(getBodyString(message))));
            return $(document).find("TRANSCODE").text();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    private static String getHeaderString(String message)
    {
        return StringUtils.left(message, MessageHeader.LENGTH);
    }

    private static String getSignString(String message)
    {
        return StringUtils.mid(message, MessageHeader.LENGTH + MessageSecurityField.SIGNATURE_LENGTH_LENGTH, getSignLength(message));
    }

    private static int getSignLength(String message)
    {
        return Integer.valueOf(StringUtils.mid(message, MessageHeader.LENGTH, MessageSecurityField.SIGNATURE_LENGTH_LENGTH));
    }

    private static String getBodyString(String message)
    {
        return StringUtils.substring(message, MessageHeader.LENGTH + MessageSecurityField.SIGNATURE_LENGTH_LENGTH + getSignLength(message));
    }

    @Override
    public String toMessage()
    {
        String bodyMessage = messageBody.toMessage();
        String sign = Utils.sign(bodyMessage, messageContext.getMerchantPrivateKey());
        MessageSecurityField securityField = new MessageSecurityField();
        securityField.setSignature(sign);

        return new StringBuilder()
                .append(messageHeader.toMessage())
                .append(securityField.toMessage())
                .append(bodyMessage)
                .toString();
    }

    @Override
    public Message<Header, Content, Body> fromMessage(MessageContext messageContext, String message)
    {
        String bodyData = getBodyString(message);
        if (!Utils.verifySign(bodyData, messageContext.getBankPublicKey(), getSignString(message)))
            throw new RuntimeException(String.format("验签失败:%s", message));
        messageHeader.fromMessage(messageContext, getHeaderString(message));
        messageBody.fromMessage(messageContext, bodyData);
        return this;
    }
}
