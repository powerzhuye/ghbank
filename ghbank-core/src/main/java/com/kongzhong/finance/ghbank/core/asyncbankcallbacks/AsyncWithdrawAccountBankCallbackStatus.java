package com.kongzhong.finance.ghbank.core.asyncbankcallbacks;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum AsyncWithdrawAccountBankCallbackStatus
{
    ORDER_COMPLETED("订单完成"),
    ORDER_PRE_AUTHING("订单预授权中");

    @Getter
    private final String desc;

}


