package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public abstract class AsyncBankCallback extends AbstractContent
{
    @ContentField(name = "OLDREQSEQNO", description = "原交易流水号")
    private String oldRequestNo;

    @ContentField(secure = false, name = "MERCHANTID", description = "商户唯一编号")
    private String merchantId;

    @ContentField(secure = false, name = "BANKID", description = "银行标识")
    private String bankId;

    @ContentField(name = "EXT_FILED1", description = "备用字段1")
    private String extraField1;

    @ContentField(name = "EXT_FILED2", description = "备用字段2")
    private String extraField2;

    @ContentField(name = "EXT_FILED3", description = "备用字段3")
    private String extraField3;


    @Override
    public InvokeMethod getInvokeMethod()
    {
        return InvokeMethod.ASYNC;
    }
}
