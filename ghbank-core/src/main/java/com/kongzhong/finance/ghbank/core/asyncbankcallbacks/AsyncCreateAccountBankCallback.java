package com.kongzhong.finance.ghbank.core.asyncbankcallbacks;

import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallback;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class AsyncCreateAccountBankCallback extends AsyncBankCallback
{
    @ContentField(name = "ACNAME", description = "姓名")
    private String accountName;

    @ContentField(name = "IDTYPE", description = "证件类型")
    private String idType;

    @ContentField(name = "IDNO", description = "证件号码")
    private String idNo;

    @ContentField(name = "MOBILE", description = "手机号码")
    private String mobile;

    @ContentField(name = "ACNO", description = "银行账号")
    private String accountNo;
}
