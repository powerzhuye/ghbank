package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
public class SyncCheckDiscardInvestProductResultBankResponseRsItem
{
    @ContentField(name = "REQSEQNO", description = "投标交易流水号")
    private String requestNo;

    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @ContentField(name = "AMOUNT", description = "投标金额")
    private String amount;

    @ContentField(name = "HOSTDT", description = "银行交易日期")
    private String transactionDate;

    @ContentField(name = "STATUS", description = "状态")
    private String status;

    @ContentField(name = "ERRORMSG", description = "错误原因")
    private String errorMessage;

    @ContentField(name = "HOSTJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;
}