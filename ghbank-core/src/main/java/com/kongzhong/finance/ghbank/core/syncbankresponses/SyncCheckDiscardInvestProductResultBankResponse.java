package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultBankResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class SyncCheckDiscardInvestProductResultBankResponse extends SyncCheckResultBankResponse
{
    @ContentField(nested = true, name = "RSLIST", description = "明细投资信息")
    private List<SyncCheckDiscardInvestProductResultBankResponseRsItem> rsItems = new ArrayList<>();

}
