package com.kongzhong.finance.ghbank.core.asyncbankcallbacks;

import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallback;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class AsyncDiscardInvestProductBankCallback extends AsyncBankCallback
{
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @ContentField(name = "CANCELREASON", description = "撤标原因")
    private String cancelReason;
}
