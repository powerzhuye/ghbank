package com.kongzhong.finance.ghbank.core.asyncbankcallbacks;

import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallback;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class AsyncDepositAccountBankCallback extends AsyncBankCallback
{
    @ContentField(name = "ORDERSTATUS", description = "订单状态")
    private AsyncDepositAccountBankCallbackStatus orderStatus;
}
