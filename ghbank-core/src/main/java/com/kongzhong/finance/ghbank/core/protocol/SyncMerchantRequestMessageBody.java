package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class SyncMerchantRequestMessageBody<Content extends SyncMerchantRequest> extends MessageBody<RequestHeader, Content>
{
    public SyncMerchantRequestMessageBody(Content content)
    {
        super(new RequestHeader(), content);
    }
}
