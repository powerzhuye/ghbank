package com.kongzhong.finance.ghbank.core.protocol;

import com.kongzhong.finance.ghbank.core.MessageContext;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public abstract class SyncCheckResultMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 28)
    @ContentField(name = "OLDREQSEQNO", description = "原交易流水号")
    private String oldRequestNo;

    public SyncCheckResultMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }
}
