package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncCheckDailyTransactionResultMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 2)
    @ContentField(name = "OPERFLAG", description = "对账类型")
    private String operationType = "0";

    @NotNull
    @Size(min = 8, max = 8)
    @ContentField(name = "CHECKDATE", description = "对账日期")
    private String checkDate;

    public SyncCheckDailyTransactionResultMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00077";
    }
}
