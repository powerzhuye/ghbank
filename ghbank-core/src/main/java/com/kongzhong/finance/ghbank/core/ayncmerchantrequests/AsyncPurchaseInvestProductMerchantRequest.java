package com.kongzhong.finance.ghbank.core.ayncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ApplicationType;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantRequest;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncPurchaseInvestProductMerchantRequest extends AsyncMerchantRequest
{
    @NotNull
    @Size(max = 128)
    @ContentField(name = "MERCHANTNAME", description = "商户名称")
    private final String merchantName;

    @NotNull
    @Size(max = 2)
    @ContentField(name = "TTRANS", description = "交易类型")
    private String operationType = AsyncMerchantRequestOperationType.互联网借贷投标.getCode();

    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "AMOUNT", description = "投标金额")
    private String amount;

    @Size(max = 60)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    public AsyncPurchaseInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);

        merchantName = messageContext.getMerchantName();
    }

    @Override
    public String getTransactionCode()
    {
        if (messageContext.getApplicationType().equals(ApplicationType.PC.toString()))
            return "OGW00052";
        else
            return "OGW00094";
    }
}
