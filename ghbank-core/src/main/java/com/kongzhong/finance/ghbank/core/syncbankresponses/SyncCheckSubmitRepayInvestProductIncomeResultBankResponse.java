package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncCheckResultBankResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuye on 21/01/2017.
 */
@Data
public class SyncCheckSubmitRepayInvestProductIncomeResultBankResponse extends SyncCheckResultBankResponse
{
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @ContentField(name = "DFFLAG", description = "还款类型")
    private String repayType;

    @ContentField(name = "BWACNAME", description = "还款账号户名")
    private String bwAccountName;

    @ContentField(name = "BWACNO", description = "还款账号")
    private String bwAccountNo;

    @ContentField(name = "TOTALNUM", description = "总笔数")
    private String totalCount;

    @ContentField(nested = true, name = "RSLIST", description = "明细收益投资人")
    private List<SyncCheckSubmitRepayInvestProductIncomeResultBankResponseRepayItem> repayItemList = new ArrayList<>();

}
