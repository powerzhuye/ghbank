package com.kongzhong.finance.ghbank.core.asyncmerchantresponses;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantResponse;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class AsyncBindCardMerchantResponse extends AsyncMerchantResponse
{
    public AsyncBindCardMerchantResponse(MessageContext messageContext)
    {
        super(messageContext);
    }
}
