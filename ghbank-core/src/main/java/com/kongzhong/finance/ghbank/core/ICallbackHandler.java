package com.kongzhong.finance.ghbank.core;

import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallback;
import com.kongzhong.finance.ghbank.core.protocol.AsyncBankCallbackMessageBody;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantResponse;
import com.kongzhong.finance.ghbank.core.protocol.ResponseException;

/**
 * Created by zhuye on 22/01/2017.
 */
public interface ICallbackHandler<
        ABC extends AsyncBankCallback, ABCM extends AsyncBankCallbackMessageBody<ABC>,
        AMR extends AsyncMerchantResponse>
{
    AMR handle(ABCM bankResponse, MessageContext context) throws ResponseException;
}
