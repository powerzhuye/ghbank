package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncBankResponse;
import lombok.Data;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncQueryAccountBalanceBankResponse extends SyncBankResponse
{
    @ContentField(name = "ACNO", description = "银行账号")
    private String accountNo;

    @ContentField(name = "ACNAME", description = "账号户名")
    private String accountName;

    @ContentField(name = "ACCTBAL", description = "账户余额")
    private String accountBalance;

    @ContentField(name = "AVAILABLEBAL", description = "可用余额")
    private String availableBalance;

    @ContentField(name = "FROZBL", description = "冻结金额")
    private String frozenBalance;

    @ContentField(name = "EXT_FILED1", description = "E账户状态")
    private String accountStatus;
}
