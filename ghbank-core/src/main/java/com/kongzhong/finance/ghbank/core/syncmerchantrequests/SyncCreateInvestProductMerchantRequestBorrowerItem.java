package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncCreateInvestProductMerchantRequestBorrowerItem
{
    @NotNull
    @Size(max = 128)
    @ContentField(name = "BWACNAME", description = "借款人姓名")
    private String accountName;

    @Size(max = 4)
    @ContentField(name = "BWIDTYPE", description = "借款人证件类型")
    private String idType = "1010";

    @Size(max = 32)
    @ContentField(name = "BWIDNO", description = "借款人证件号码")
    private String idNo;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "BWACNO", description = "借款人账号")
    private String accountNo;

    @Size(max = 64)
    @ContentField(name = "BWACBANKID", description = "借款人账号所属行号")
    private String bankId;

    @Size(max = 256)
    @ContentField(name = "BWACBANKNAME", description = "借款人账号所属行名")
    private String bankName;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "BWAMT", description = "借款人金额")
    private String amount;

    @Size(max = 128)
    @ContentField(name = "MORTGAGEID", description = "借款人抵押品编号")
    private String mortgageId;

    @Size(max = 1024)
    @ContentField(name = "MORTGAGEINFO", description = "借款人抵押品简单描述")
    private String mortgageDesc;

    @Size(min = 8, max = 8)
    @ContentField(name = "CHECKDATE", description = "借款人审批通过日期")
    private String checkDate;

    @Size(max = 1028)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;
}
