package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Created by zhuye on 16/01/2017.
 */
@RequiredArgsConstructor
public enum Priority
{
    NORMAL(1),
    URGENT(2),
    VERY_URGENT(3);

    @Getter
    private final int code;

    public static Priority parse(int code)
    {
        return Arrays.stream(values()).filter(item -> item.code == code)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s中找不到code:%d", Priority.class.getName(), code)));
    }
}


