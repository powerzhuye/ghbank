package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncMerchantResponseMessageBody<Content extends AsyncMerchantResponse> extends MessageBody<ResponseHeader, Content>
{
    public AsyncMerchantResponseMessageBody(Content content)
    {
        super(new ResponseHeader(), content);
    }
}
