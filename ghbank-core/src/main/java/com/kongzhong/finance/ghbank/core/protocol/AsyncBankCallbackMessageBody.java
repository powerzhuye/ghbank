package com.kongzhong.finance.ghbank.core.protocol;

import lombok.Data;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncBankCallbackMessageBody<Content extends AsyncBankCallback> extends MessageBody<ResponseHeader, Content>
{
    public AsyncBankCallbackMessageBody(Content content)
    {
        super(new ResponseHeader(), content);
    }
}
