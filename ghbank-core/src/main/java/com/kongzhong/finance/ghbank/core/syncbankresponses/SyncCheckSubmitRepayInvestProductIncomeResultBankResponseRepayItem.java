package com.kongzhong.finance.ghbank.core.syncbankresponses;

import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

/**
 * Created by zhuye on 04/02/2017.
 */
@Data
public class SyncCheckSubmitRepayInvestProductIncomeResultBankResponseRepayItem
{
    @ContentField(name = "SUBSEQNO", description = "子流水号")
    private String subSequenceNo;

    @ContentField(name = "ACNO", description = "投资人账号")
    private String accountNo;

    @ContentField(name = "ACNAME", description = "投资人账号户名")
    private String accountName;

    @ContentField(name = "INCOMEDATE", description = "该收益所属截止日期")
    private String incomeDate;

    @ContentField(name = "AMOUNT", description = "还款总金额")
    private String amount;

    @ContentField(name = "PRINCIPALAMT", description = "本次还款本金")
    private String principalAmount;

    @ContentField(name = "INCOMEAMT", description = "本次还款收益")
    private String incomeAmount;

    @ContentField(name = "FEEAMT", description = "本次还款费用")
    private String feeAmount;

    @ContentField(name = "ERRORMSG", description = "错误原因")
    private String errorMessage;

    @ContentField(name = "HOSTJNLNO", description = "银行交易流水号")
    private String bankTransactionNo;

    @ContentField(name = "HOSTDT", description = "银行交易日期")
    private String transactionDate;

    @ContentField(name = "STATUS", description = "状态")
    private String status;

}