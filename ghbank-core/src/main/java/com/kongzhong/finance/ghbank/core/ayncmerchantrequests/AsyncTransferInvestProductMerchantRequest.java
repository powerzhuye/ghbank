package com.kongzhong.finance.ghbank.core.ayncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ApplicationType;
import com.kongzhong.finance.ghbank.core.protocol.AsyncMerchantRequest;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 16/01/2017.
 */
@Data
public class AsyncTransferInvestProductMerchantRequest extends AsyncMerchantRequest
{
    @NotNull
    @Size(max = 2)
    @ContentField(name = "TTRANS", description = "交易类型")
    private String operationType = AsyncMerchantRequestOperationType.债券转让.getCode();

    @NotNull
    @Size(max = 28)
    @ContentField(name = "OLDREQSEQNO", description = "原投标流水")
    private String oldPurchaseRequestNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "OLDREQNUMBER", description = "原标的编号")
    private String oldProductId;

    @NotNull
    @Size(max = 512)
    @ContentField(name = "OLDREQNAME", description = "原标的名称")
    private String oldProductName;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "ACCNO", description = "转让人银行账号")
    private String accountNo;

    @NotNull
    @Size(max = 64)
    @ContentField(name = "CUSTNAME", description = "转让人名称")
    private String accountName;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "PREINCOME", description = "预计剩余收益")
    private String predictIncome;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "AMOUNT", description = "剩余金额")
    private String amount;


    @Size(max = 60)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    public AsyncTransferInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);
    }

    @Override
    public String getTransactionCode()
    {
        if (messageContext.getApplicationType().equals(ApplicationType.PC.toString()))
            return "OGW00061";
        else
            return "OGW00096";
    }
}
