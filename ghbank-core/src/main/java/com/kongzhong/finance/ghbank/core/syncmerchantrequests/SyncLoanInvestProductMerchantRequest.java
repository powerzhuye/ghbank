package com.kongzhong.finance.ghbank.core.syncmerchantrequests;

import com.kongzhong.finance.ghbank.core.MessageContext;
import com.kongzhong.finance.ghbank.core.protocol.ContentField;
import com.kongzhong.finance.ghbank.core.protocol.SyncMerchantRequest;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by zhuye on 22/01/2017.
 */
@Data
public class SyncLoanInvestProductMerchantRequest extends SyncMerchantRequest
{
    @NotNull
    @Size(max = 128)
    @ContentField(name = "MERCHANTNAME", description = "商户名称")
    private final String merchantName;

    @NotNull
    @Size(max = 64)
    @ContentField(name = "LOANNO", description = "借款编号")
    private String loanNo;

    @NotNull
    @Size(max = 128)
    @ContentField(name = "BWACNAME", description = "借款人姓名")
    private String bwAccountName;

    @NotNull
    @Size(max = 32)
    @ContentField(name = "BWACNO", description = "借款人账号")
    private String bwAccountNo;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "ACMNGAMT", description = "账户管理费")
    private String accountManageAmount;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    @ContentField(name = "GUARANTAMT", description = "风险保证金")
    private String guaranteeAmount;

    @Size(max = 60)
    @ContentField(name = "REMARK", description = "备注")
    private String remark;

    public SyncLoanInvestProductMerchantRequest(MessageContext messageContext)
    {
        super(messageContext);

        merchantName = messageContext.getMerchantName();
    }

    @Override
    public String getTransactionCode()
    {
        return "OGW00065";
    }
}
